for (package in c('data.table', 'plyr', 'dplyr', 'e1071','gbm','kernlab','MCMCglmm','lme4','nloptr','reshape2','plotly',
                        'lme4','car','ppcor','jsonlite','scales','ggplot2', 'DT','ROCR', 'DMwR','randomForest', 'xgboost', 
                        'caTools','caret','caretEnsemble','doParallel','pROC')) 
{
  if (suppressMessages(!require(package, character.only=T, quietly=T))) 
  {
    install.packages(package)
  }
}