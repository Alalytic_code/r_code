
library(jsonlite)
args = commandArgs(trailingOnly = TRUE)
args = URLdecode(args)

args = fromJSON(args)
print(args)
args$res = list()
args$req = list()



rmodels_path = args['rmodels_path']
source(paste(rmodels_path, 'module_loader.r', sep="/"))
result = do.call(run, args)

outdir = args['outdir']
filename = "cmd_output.json"
filepath = paste(outdir, filename , sep="/")
output = toJSON(result, auto_unbox = TRUE)
print(output)
write(output, filepath)