##############################################################################
# Module: Channel Attributor                                                 #
# Code: prep.R                                                               #
# Objective: Prepare modeling dataset, applying transformations              #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

stime = as.POSIXct(Sys.time())

########################################### Start Definitions ########################################
#Calculate *monthly* seasonality factor from market sales
#x=data frame containing geobrand, mth_num and mktsales (in that order)
seasfact = function(df) {
  m = group_by(df, geobrandcol, mth_num) %>% summarise_each_(funs(m=mean(.)), mktsales) #avg mktsales by month (1-12)
  y = summarise_each_(df, funs(y=mean(.)), mktsales)                                    #global avg mktsales
  seasdf = merge(m, y)
  seasdf$seasfact = seasdf$m/seasdf$y                                                   #seasfact = month avg/global avg
  seasdf = seasdf[!names(seasdf) %in% c('m', 'y')]
  return(seasdf)
}

#Apply Adstock Decay
#x=data frame containing id and var | lambda=decay rate
decay = function(x, lambda) {
  dk = numeric(nrow(x))
  dk[1] = x[1, 2]
  first.id = c(TRUE, !x[-nrow(x), 1] == x[-1, 1]) #boolean vector for by-group processing
  
  for (i in 2:nrow(x)) {
    if (first.id[i]) dk[i] = x[i, 2] else dk[i] = lambda*x[i, 2] + (1-lambda)*dk[i-1]
  }
  return(dk)
}

#standardize a column into [0,1] range
#x=column to be standardized | minx=min of column | rngx=range of column
stand = function(x, minx, rngx) {
  return( ifelse(rngx>0, (x - minx)/rngx, x) )
}

#plot model variables time trend
SeasPlotFun = function(var) {
  plot_obj = 
  ungroup(seasplot) %>% mutate(value=seasplot[[var]], period1=as.POSIXct(periodcol)) %>% 
  ggplot(aes(x = period1, y = value, colour = geobrandcol)) +
  facet_wrap(~geobrandcol, scales = "free", ncol = 2) +
  geom_line(stat = 'identity', size = 0.8, linetype = 1) +
  ggtitle(paste('Seasonality Plot for', var))

  #export to JSON
  name = ifelse(var=='depvarcol', depvar, var)
  title = paste('Seasonality Plot for <span>', toupper(name),'</span>')
  xAxis = list(label=period, tickFormat=tickFormat, type="date")
  yAxis = list(label=name)
  plot_to_json(paste0('Seasonality_',name),
               plot_obj,
               category="Seasonality",
               chart_type = 'scatter',
               xAxis = xAxis,
               yAxis = yAxis,
               title = title,
               write_file = TRUE )
  return(plot_obj) 
}
########################################### End Definitions ########################################

load(file = paste0(outdir, '/read.rda'))

pdata = rdata
use_log1 = as.integer(use_log)
use_mc1 = as.integer(use_mc)

cat("\n", '==================================================')
cat("\n", '          ***** DATA PREPARATION ***** ')
cat("\n", '==================================================', "\n")

#run seasonality plot
seasplot = group_by(pdata, geobrandcol, periodcol) %>% summarise_each_(funs(sum(.)), c('depvarcol', lst_indep1))
date_interval = as.integer(seasplot$periodcol[2] - seasplot$periodcol[1])
if (date_interval <= 7) tickFormat = '%b-%d-%Y' else  tickFormat = '%b-%Y'
pplot1 = lapply(c('depvarcol', lst_indep1), SeasPlotFun)

#create trend variable
pdata$trend = as.numeric(as.factor(pdata$periodcol))

#seas as period dummies
pdata$prd_num = format(as.Date(pdata$periodcol), "%Y%m%d")
lst_prd_num = sort(unique(pdata$prd_num))
lst_dummy = paste0('D', lst_prd_num)
for (l in lst_prd_num) pdata[, paste0('D', l)] = ifelse(pdata$prd_num == l, 1, 0)

#list of monthly dummies to be used based on seas_cutoff
seasplot$group = c(TRUE, !seasplot[-nrow(seasplot), 'geobrandcol'] == seasplot[-1, 'geobrandcol'])
seasplot$abs_pct_chg = ifelse(seasplot$group, 0, 100 * abs((seasplot$depvarcol/lag(seasplot$depvarcol) - 1)))
lst_seas = mlist(lst_geobrand)

#if abs change from previous period is higher than seas_cutoff, include dummy in lst_seas
seas_cutoff1 = as.numeric(seas_cutoff) 
cat('\n', paste0('Seasonality cutoff = ', seas_cutoff1, '%'), '\n')
for (gb in lst_geobrand) {
  foo = seasplot[seasplot$geobrandcol == gb, ]
  lst_seas[[gb]] = lst_dummy[which(foo$abs_pct_chg > seas_cutoff1)]
  cat('\n', paste('Seasonality dummies for Brand/Geo', gb), '\n')
  if (length(lst_seas[[gb]]) > 0) cat(lst_seas[[gb]], '\n') else cat('None', '\n')
}

#add competitor sales and seasonality factor (if market sales provided by user)
if (!is.null(mktsales)) {
  pdata$compt = pdata[[mktsales]] - pdata$depvarcol #competitor sales = market - target brand 
  pdata$mth_num = format(as.Date(pdata$periodcol), '%m')
  seas = seasfact(select_(pdata, 'geobrandcol', 'mth_num', mktsales))
  pdata = merge(pdata, seas, by=c('geobrandcol', 'mth_num'))
}

#create a column combining geobrand and subject (required for decay and mean centering)
pdata$geobrandsub = paste(pdata$geobrandcol, pdata$subjectcol, sep = '.')

############################################# Adstock Decay ############################################
#applied to indep vars only
usedecaylabel = ifelse(as.integer(use_decay) == 1, TRUE, FALSE)
cat('\n', paste( 'decay is set to', usedecaylabel), '\n')

if (as.integer(use_decay) == 1)
{
  dkbounds1 = str_to_lst(decay_bounds)
  min_decay = as.numeric(dkbounds1[1])/100
  max_decay = as.numeric(dkbounds1[2])/100
  lst_lambda = seq(min_decay, max_decay, 0.1)
  cat('\n', paste( 'decay rates from', (min_decay)*100, 'to', (max_decay)*100, 'will be modeled'), '\n')
  
  #expand to all subject-all period and sort by subject-period
  comb = expand.grid(unique(pdata$geobrandsub), unique(pdata$periodcol))
  names(comb) = c('geobrandsub', 'periodcol')
  data1 = merge(comb, pdata, by=c('geobrandsub', 'periodcol'), all.x = TRUE)
  data1[is.na(data1)] = 0
  data1 = arrange(data1, geobrandsub, periodcol)

  #initialize a temp data frame 
  lst_var = as.vector(t(outer(lst_indep1, lst_lambda*10, paste0)))
  tempdf = data.frame( matrix( NA, ncol = length(lst_var), nrow = nrow(data1) ) )
  names(tempdf) = lst_var
  
  #v=variable, l=lambda
  i = 1
  for (v in lst_indep1) {
    for (l in lst_lambda) {
      tempdf[i] = decay(data1[, c('geobrandsub', v)], l)
      i = i + 1
    }
  }
  
  data1 = cbind(data1, tempdf)
  foo = pdata[, c('geobrandsub', 'periodcol')]
  pdata = merge(data1, foo, by=c('geobrandsub', 'periodcol')) #drop extra records

} else {
  lst_lambda = 0
  lst_var = lst_indep1
}

############################################# Log Transformation ############################################
#when use_log=1 applied to indep vars only | when use_log=2 applied to depvar & indep vars
if (use_log1==0) useloglabel = 'Linear: Y = a + b * X' else
if (use_log1==1) useloglabel = 'Semi-log: Y = a + b * log(X)' else
                 useloglabel = 'Log-linear: log(Y) = a + b * log(X)'

cat('\n', paste(  'Model Form', useloglabel), '\n')

if (use_log1 > 0) {
  if (use_log1 == 1) {
    pdata = mutate_each_(pdata, funs(logtr(.)), lst_var) 
  } else {
    pdata = mutate_each_(pdata, funs(logtr(.)), c('depvarcol', lst_var))
  }
}

################################################# Mean Centering ############################################
#mean-centering: applied to depvar & indep vars | min-max: applied to indep vars only
if (use_mc1 == 0) usemclabel = 'No standardization done' else
if (use_mc1 == 1) usemclabel = 'Standardization using mean-centering: x - mean(x)' else 
                  usemclabel = 'Standardization using min-max: (x - min(x))/(max(x) - min(x))'
cat('\n', usemclabel, '\n')

#list of variables to be standardized
if (use_mc1 == 1) {
  if (!is.null(mktsales)) lst_var1 = c('depvarcol', lst_var, 'compt') else lst_var1 = c('depvarcol', lst_var)
} else {
  if (!is.null(mktsales)) lst_var1 = c(lst_var, 'compt') else lst_var1 = lst_var
}

if (use_mc1 == 1) {
  means = group_by(pdata, geobrandsub) %>% summarise_each_(funs(mean = mean(.)), lst_var1) 
  lst_mean = names(means[, -1])
  pdata = merge(pdata, means, by='geobrandsub')
  pdata[, lst_var1] = pdata[, lst_var1] - pdata[, lst_mean]

} else if (use_mc1 == 2) {
  mins = group_by(pdata, geobrandsub) %>% summarise_each_(funs(min = min(.)), lst_var1) #min of vars
  rngs = group_by(pdata, geobrandsub) %>% summarise_each_(funs(rng = max(.)), lst_var1) #max of vars
  rngs = cbind(rngs[, 1], rngs[, -1] - mins[, -1])                                      #max minus min
  lst_min = names(mins[ ,-1])
  lst_rng = names(rngs[, -1])
  
  pdata = merge( merge(pdata, mins, by='geobrandsub'), rngs, by='geobrandsub' )
  
  #standardize the variables with non-zero range
  for (i in 1:length(lst_var1)) {
    pdata[, lst_var1[i]] = stand(x=pdata[, lst_var1[i]], minx=pdata[, lst_min[i]], rngx=pdata[, lst_rng[i]])
  }
  pdata = pdata[!names(pdata) %in% c(lst_min, lst_rng)] #min and range columns not required beyond this stage
}

pdata = dplyr::select(pdata, -prd_num, -mth_num, -geobrandsub)  #get rid of temp columns

#Exit Prep
save(pdata, 
     geobrand, 
     depvar, 
     lst_indep1,
     lst_fac,
     subject, 
     period, 
     datefmt, 
     use_log1, 
     use_mc1,
     lst_geobrand,
     lst_seas, 
     lst_lambda,
     mktsales,
     total_cost,
     unit_cost,
     file = paste0(outdir, '/prep.rda'))
if (exists('lst_mean')) append(lst_mean, paste0(outdir, '/prep.rda'))

etime = as.POSIXct(Sys.time())
cat( '\n', paste( 'Data Preparation took', round( difftime( etime, stime, units = "mins"), 2), 'minutes' ), '\n' )
