##############################################################################
# Module: Channel Attributor                                                 #
# Code: model2.R                                                             #
# Objective: Run MCMC model on best model data, produce final outputs        #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

stime = as.POSIXct(Sys.time())

suppressWarnings(suppressMessages(library(MCMCglmm)))      #MCMCglmm

source(file = 'funmod.R', verbose = FALSE, echo = FALSE)

######################################## Section 1: Definitions ###################################
runMCMC = function(iCovStr) {
  #set core MCMC parameters
  min_V  = 0.01      #low variance: applied to channel vars for which user-specified priors exist
  max_V  = 1000      #high variance: applied to intercept, seas, trend and channels w/o user-specified priors
  nitt   = 6500      #number of mcmc iterations (default 13000)
  thin   = 5         #every x iteration will be used (default 10)
  burnin = 1500      #discard first x iterations (default 3000)

  #create model formula based on covariance structure option
  #idh = uncorrelated random slopes | us = correlated random slopes
  form_fxd = as.formula(paste('depvarcol', paste0(c(lst_fxdvars, trend1, seas, compt1, lst_fac), collapse = ' + '), sep = ' ~ '))
  form_rnd = as.formula(ifelse(iCovStr==2, paste0(' ~ us(', paste(lst_rndvars, collapse=' + '), '):', 'subjectcol'),
                                           paste0(' ~ idh(', paste(lst_rndvars, collapse=' + '), '):', 'subjectcol')))
  
  #fixed effect priors on intercept, seas and trend have mean 0 
  #fixed effect priors on channel vars are user-specified (0 interpreted as no prior)
  #order of variables: intercept > indep vars > trend > seas > competitor > factors
  n_fxd = 1+length(lst_fxdvars)
  n_rnd = length(lst_rndvars)
  n_ctrl = length(seas) + length(trend1) + length(compt1) + length(lst_fac)
  mu = as.numeric( c(0, lst_prior2, rep(0, n_ctrl)) )             #means of fixed effect priors
  sigma = numeric(length(lst_prior2))                             #variances of fixed effect priors
  for (i in 1:length(lst_prior2)) {
    prior = lst_prior2[[i]]
    sigma[i] = ifelse(prior==0, max_V, min_V*prior)               #scale variances by prior means
  }

  #VB: covariance matrix for fixed effect priors | VG: G-matrix for random effects
  #weak priors imposed on random effects
  VB = diag(n_fxd + n_ctrl) * c(max_V, sigma, rep(max_V, n_ctrl))
  VG = diag(n_rnd)

  #run MCMCglmm
  set.seed(1)

  modobj = MCMCglmm(fixed = form_fxd, random = form_rnd, data = mdata,
                    prior = list(B = list(mu = mu, V = VB),
                                 R = list(V = 1, nu = min_V),
                                 G = list(G1 = list(V = VG, nu = min_V))),
                    nitt = nitt, thin = thin, burnin = burnin, pr = TRUE, verbose = FALSE
                   ) 

  return(modobj)
}

#extract fixed effect estimates from an MCMC object
fxdMCMC = function(model) {
  fixed = data.frame(model$Sol)[, lst_fxdvars]
  fixed = apply(fixed, 2, mean) 
  fixed[fixed < 0] = 0 #set negative estimates to zero
  names(fixed) = paste0('fxd_', lst_indep2[[gb]])
  return(fixed)
}

#extract random effect estimates from an MCMC object
rndMCMC = function(model) {
  foo = data.frame(model$Sol) 
  foo = foo[!names(foo) %in% c(lst_fxdvars, seas, trend1, compt1)][, -1]
  foo = apply(foo, 2, mean)
  
  #column names formatted as "variable.subject.subject-value"
  rand = data.frame(var=sapply(strsplit(names(foo), "[.]"), "[", 1), 
                    sub=sapply(strsplit(names(foo), "[.]"), "[", 3), 
                    val=unname(foo))
  
  #fill in NAs for missing random estimates
  comb = expand.grid(lst_rndvars, t(lst_geobrandsub[[gb]]))
  names(comb) = c('var', 'sub')
  rand = merge(comb, rand, by=c('var', 'sub'), all.x = TRUE)
  
  #convert to wide format with var going across
  rand = reshape(rand, idvar = 'sub', timevar = 'var', direction = 'wide')
  rand[is.na(rand)] = 0
  names(rand) = c('subjectcol', paste0('rnd_', lst_rand2[[gb]]))
  
  #add fixed and random effect estimates
  fxd_rnd = fixed[paste0('fxd_', lst_rand2[[gb]])]
  rand = mutate_if(rand, is.numeric, funs(ifelse(. < 0, 0, .))) #set negative estimates to zero
  return(rand)
}

#converts a two-level list to data frame
lst_to_df = function(list) {
  df = data.frame(geobrand=character(0), variable=character(0), value=numeric(0))
  for (i in 1:length(list)) df = rbind(df, data.frame(geobrand = names(list[i]), 
                                                      variable = names(list[[i]]), 
                                                      value = list[[i]]))
  rownames(df) = NULL
  return(df)
}

########################################### Section 2: MCMC Run ###############################################
load(file = paste0(outdir, '/model1.rda'))

cat("\n", '===============================================================')
cat("\n", '           ***** CONSTRAINED MIXED MODEL ***** ')
cat("\n", '===============================================================', "\n")

#list of model vars to be dropped for each geobrand
drop_vars1 = rlist(drop_vars, lst_geobrand, lst_indep1)
for (gb in lst_geobrand) drop_vars1[[gb]] = names(drop_vars1[[gb]][drop_vars1[[gb]] == 1])

#modify model varlist to exclude user-dropped vars
for (gb in lst_geobrand) {
  lst_indep2[[gb]] = lst_indep2[[gb]][!lst_indep2[[gb]] %in% drop_vars1[[gb]]]
  lst_rand2[[gb]] = intersect(lst_rand2[[gb]], lst_indep2[[gb]])
}

#set fixed effect priors for each geobrand
lst_prior1 = rlist(lst_prior, lst_geobrand, lst_indep1)

#initialize list objects to store MCMC results
contribMCMC = roiMCMC = incrPrd = incrSub = fitPrd = fxdOpt = mlist(lst_geobrand)

#run MCMC for each geobrand using best model parameters
for (gb in lst_geobrand) {
  iParam = PARAMS[PARAMS$geobrand==gb & PARAMS$Model==best_model[[gb]], ]
  setParams(gb = iParam$geobrand, 
            dk = iParam$Lambda, 
            ts = iParam$TrendSeas, 
            fxdvars = lst_indep2[[gb]], 
            rndvars = lst_rand2[[gb]])

  lst_prior2 = lst_prior1[[gb]][lst_indep2[[gb]]]                     #filter for gb & exclude user-dropped vars

  #ensure alignment with LMER in case no priors supplied by user
  if (sum(lst_prior2) == 0) lst_prior2 = best_est[[gb]][lst_indep2[[gb]]] 
  
  total_cost2 = total_cost1[[tolower(gb)]][tolower(lst_indep2[[gb]])] #filter for gb & exclude user-dropped vars

  #run MCMCglmm
  class(mdata) = 'data.frame' #to avoid warnings related to tbl_df & tbl class
  modMCMC = runMCMC(iParam$CovStr)

  #print MCMC model estimates
  cat('\n', '---------------------------------------------------------------')
  cat('\n', paste('                 Model Summary for', gb, '               '))
  cat('\n', '---------------------------------------------------------------', '\n')
  
  resMCMC = data.frame(summary(modMCMC)$solutions)[-5] #don't print p-values
  resMCMC$variable = rownames(resMCMC)
  names(resMCMC) = c('estimate', 'lower_95_CI', 'upper_95_CI', 'sample_size', 'variable')
  resMCMC = dplyr::select(resMCMC, variable, sample_size, estimate, lower_95_CI, upper_95_CI)
  print(resMCMC, row.names=F) 
  
  #decomps at observation level
  pred = predict(modMCMC, marginal = NULL) #MCMC pred does not add up to actual!
  fixed = fxdMCMC(modMCMC)
  keepvars = c('depvarcol', 'pred', 'periodcol', lst_fxdvars, names(fixed), mean_y, lst_mean_fxd)
  data1 = cbind(mdata, t(fixed), pred=untransform(pred))[, keepvars]
  resDecomp = decomp(df=data1, xcols=lst_fxdvars, ecols=names(fixed), meanx=lst_mean_fxd)

  #channel % contributions
  incrMCMC = colSums(resDecomp[, lst_fxdvars])
  incr_overall = round(100 * (incrMCMC / sum(resDecomp$pred)), 2)
  incr_overall = c(100 - sum(incr_overall), incr_overall) #base = 100 - total incr
  names(incr_overall) = c('base', lst_indep2[[gb]])
  contribMCMC[[gb]] = incr_overall
  
  #channel ROI
  roiMCMC[[gb]] = getROI(incr=incrMCMC, margin=lst_margin1[[gb]], spend=total_cost2)
  names(roiMCMC[[gb]]) = lst_indep2[[gb]]
  
  #model estimates required for optimization
  fxdOpt[[gb]] = fixed
  names(fxdOpt[[gb]]) = lst_indep2[[gb]]
  
  #print overall results
  cat( '\n', '----- Channel Contributions -----', '\n' ) 
  print(contribMCMC[[gb]])
  cat( '\n', paste0('Total Incremental Units = ', round(sum(contribMCMC[[gb]][-1]), 2), '%' ), '\n' )
  cat( '\n', '----- Channel ROI -----', '\n' ) 
  print(roiMCMC[[gb]])

  #decomps by period - required for area chart
  incr_by_prd = group_by(resDecomp, periodcol) %>% summarise_each_(funs(sum(.)), c('pred', lst_fxdvars))
  incr_by_prd$base = incr_by_prd$pred - rowSums(incr_by_prd[, lst_fxdvars])  #base = pred minus sum of channels
  names(incr_by_prd) = c('periodcol', 'pred', lst_indep2[[gb]], 'base')      #remove decay rate from col names
  incrPrd[[gb]] = incr_by_prd
  
  #actual & pred Y by period - required for fit chart
  pred_by_prd = mutate(resDecomp, depvarcol=untransform(depvarcol)) %>% 
    group_by(periodcol) %>% summarise(pred=sum(pred), actual=sum(depvarcol))
  fitPrd[[gb]] = pred_by_prd
  
  #decomps by subject
  rand = rndMCMC(modMCMC)
  keepvars = c('depvarcol', lst_rndvars, names(rand), mean_y, lst_mean_rnd)
  data1 = merge(mdata, rand, by = 'subjectcol')[, keepvars]
  resDecomp = decomp(df=data1, xcols=lst_rndvars, ecols=names(rand)[-1], meanx=lst_mean_rnd)
  
  #adjustment to make subject level incr add up to overall incr
  foo = group_by(resDecomp, subjectcol) %>% summarise_each_(funs(sum(.)), lst_rndvars)
  incr_by_sub = colSums(foo[, -1])
  rnd_incr = incrMCMC[lst_rndvars]
  factor = ifelse(is.nan(rnd_incr/incr_by_sub), 0, rnd_incr/incr_by_sub)
  foo[, -1] = sweep(data.frame(foo[, -1]), 2, factor, FUN='*')
  incrSub[[gb]] = foo
}

######################################## Section 3: Final Outputs & Plots ###########################################
#write final outputs to CSV
contribMCMC = lst_to_df(contribMCMC)
write_csv("model_overall", contribMCMC, row.names = FALSE, write_file=TRUE)
roiMCMC = lst_to_df(roiMCMC)
write_csv("model_ROI", roiMCMC, row.names = FALSE, write_file=TRUE)
fxdOpt = lst_to_df(fxdOpt)
write_csv("model_estimates", fxdOpt, row.names = FALSE, write_file=TRUE)

#convert area and fit chart data to long format
areaData = fitData = data.frame()
for (gb in lst_geobrand) {
  foo = melt(incrPrd[[gb]], id.vars = 'periodcol', variable.name = 'channel') 
  foo$geobrand = gb
  areaData = rbind(areaData, foo)
  areaData = filter(areaData, !channel == 'pred')
  
  foo = melt(fitPrd[[gb]], id.vars = 'periodcol', variable.name = 'depvarcol') 
  foo$geobrand = gb
  fitData = rbind(fitData, foo)
}

#for subject level, a separate CSV file for each geobrand
for (gb in lst_geobrand) {
  foo = incrSub[[gb]]
  names(foo) = c('subjectcol', lst_rand2[[gb]])
  foo$total = rowSums(foo[, -1])
  write_csv(paste0('model_subject_', gb), foo, row.names = FALSE, write_file=TRUE)
}

#ROI bar chart - pass JSON to UI
plotdata = 
  rename(roiMCMC, x=variable, y=value, group=geobrand) %>%
    filter(y>0, y<Inf)

write_json(list(
                data = plotdata,
                title = paste('Channel ROI by', geobrand),
                xAxis = list(label = 'Channel'),
                yAxis = list(label = 'ROI'),
                type = 'bar',
                name = 'ROI_Bar_Chart',
                category = 'ROI',
                viewMode = 'carousel'
               ), path = paste0(outdir, '/roi_plot_json.json'))
plotdata$type='bar'
json_toPlot(plot_name ='Channel ROI by Geo by Brand',
            plot_object = list(
                               data = plotdata,
                               title = paste('Channel ROI by', geobrand),
                               xAxis = list(label = 'Channel'),
                               yAxis = list(label = 'ROI'),
                               type = 'bar',
                               name = 'ROI_Bar_Chart',
                               category = 'ROI',
                               viewMode = 'carousel'
           ), category = 'ROI',viewMode = 'carousel')

# m2plot1 =
#   arrange(roiMCMC, geobrand, -value) %>%
#   ggplot(aes(x = variable, y = value, fill = geobrand)) +
#   facet_wrap(~ geobrand, scales = "free", ncol=2) +
#   geom_bar(stat = 'identity') +
#   ggtitle(paste('Channel ROI for Geo/Brand', geobrand)) +
#   labs(x = 'Channel', y = 'ROI')
# 
#   #export plot to json
#   title = paste('Channel ROI for Geo/Brand')
#   xAxis = list(label='VARIABLES')
#   yAxis = list(label='')
#   ROI_bar_chart=plot_to_json('ROI_bar_chart',
#                              m2plot1,
#                              category = "ROI",
#                              chart_type = 'groupChart',
#                              viewMode = 'carousel',
#                              xAxis = xAxis,
#                              yAxis = yAxis,
#                              title = title,
#                              write_file = TRUE)

#contribs waterfall
m2plot2 = 
  arrange(contribMCMC, geobrand, -value) %>% group_by(geobrand) %>% 
  mutate(id=seq_along(value), 
         cume=cumsum(value), 
         lcume=lag(cume), 
         variable=factor(variable, levels=unique(variable))) %>%
  mutate_each(funs(ifelse(is.na(.), 0, .)), lcume) %>%
  ggplot() + 
  facet_wrap(~ geobrand, scales="free", ncol=2) +
  geom_rect(aes(x=variable, xmin=id-0.45, xmax=id+0.45, ymin=cume, ymax=lcume, fill=geobrand)) + 
  ggtitle('Channel Contributions Waterfall Chart') + 
  labs(x = 'Channel', y = '% Contribution')
  
  #export plot to json 
  title = paste('Channel Contributions Waterfall Chart')
  xAxis = list(label='Channel')
  yAxis = list(label='% Contribution')
  waterfall_chart=plot_to_json('waterfall_chart',
                                chart_type = 'bar',
                                m2plot2,
                                viewMode = 'carousel',
                                xAxis = xAxis,
                                yAxis = yAxis,
                                title = title,
                                category = "Waterfall",
                                write_file = TRUE)

#area chart - incremental units by period
m2plot3 = 
  arrange(areaData, geobrand, periodcol, value) %>%
  mutate(channel=factor(channel, levels=sort(unique(channel))), periodcol=as.POSIXct(periodcol)) %>%
  ggplot(aes(x = periodcol, y = value, fill = channel)) + 
  facet_wrap(~geobrand, scales = "free", ncol = 2) +
  geom_area(colour = "black", size = 0.2, stat = "identity", position = position_stack(reverse=T)) + 
  ggtitle(paste('Channel Contributions by', period)) +
  # scale_x_datetime(date_labels = "%Y-%m-%d", breaks = date_breaks("30 day")) +
  labs(x = 'Channel', y = 'Incremental Units')

areaData=arrange(areaData, geobrand, periodcol, value)
plotAreaData= rename(areaData,x=periodcol, y=value, group=geobrand,name=channel)
plotAreaData$x= as.numeric(as.POSIXct(plotAreaData$x))


write_json(list(
    data = plotAreaData,
    title = paste('Channel Contributions by', period),
    xAxis = list(label=period, tickFormat="%b-%d-%Y", type="date"),
    yAxis = list(label='Incremental Units'),
    type = 'area_chart',
    name = 'Channel Contributions',
    viewMode = 'carousel',
    category = 'Channel Contributions'
  ), path = paste0(outdir, '/Area_plot_json.json'))
  
plotAreaData$type='scatter'
plotAreaData$mode='lines'
plotAreaData$fill='tonexty'
json_toPlot(plot_name = paste('Channel Contributions by', period),
            plot_object = list(
                              data = plotAreaData,
                              title = paste('Channel Contributions by', period),
                              xAxis = list(label=period, tickFormat="%b-%d-%Y", type="date"),
                              yAxis = list(label='Incremental Units'),
                              name = 'Channel Contributions',
                              viewMode = 'carousel'
                              ),
            category = 'Channel Contributions')
  #export plot to json 
  
  # title = paste('Channel Contributions by', period)
  # xAxis = list(label=period, tickFormat="%b-%d-%Y", type="date")
  # yAxis = list(label='Incremental Units')
  # 
  
  # area_chart=plot_to_json('area_chart',
  #                               m2plot3,
  #                               chart_type = 'area_chart',
  #                               category = "Channel Contributions",
  #                               #geobrand_num = length(sort(unique(areaData$geobrand))),
  #                               #channel_num = length(sort(unique(areaData$channel))),
  #                               viewMode = 'carousel',
  #                               xAxis = xAxis,
  #                               yAxis = yAxis,
  #                               title = title,
  #                               write_file = TRUE)

#fit chart - actual vs predicted depvar by period
fitData$R2 = NA
for (gb in lst_geobrand) fitData[fitData$geobrand==gb, ]$R2 = 
  RSquared(Target = fitData[fitData$geobrand==gb & fitData$depvarcol=='actual', 'value'],
           Predicted = fitData[fitData$geobrand==gb & fitData$depvarcol=='pred', 'value'])
fitData$label = paste0(fitData$geobrand, ' (R-squared=', round(fitData$R2, 1), ')') #add R2 to panel labels

m2plot4 = 
  mutate(fitData, periodcol=as.POSIXct(periodcol)) %>%
  ggplot(aes(x = periodcol, y = value, colour = depvarcol)) +
  facet_wrap(~geobrand, scales = "free", ncol = 2) +
  geom_line(stat = 'identity') + 
  ggtitle(paste('Fit Chart: Actual vs Predicted', depvar)) +
  labs(x = period, y = depvar)

  #export plot to JSON
  title = paste('Fit Chart: Actual vs Predicted', depvar)
  xAxis = list(label=period, tickFormat="%b-%d-%Y", type="date")
  yAxis = list(label='')
  fit_chart=plot_to_json_fit('fit_chart',
                             chart_type = 'fit_chart', 
                             category = "Fit", 
                             m2plot4,
                             viewMode = 'carousel',
                             xAxis = xAxis,
                             yAxis = yAxis,
                             title = title, 
                             write_file = TRUE)

#Exit Model2
save(pdata, 
     fxdOpt, 
     unit_cost,
     lst_margin1,
     geobrand,
     depvar,
     subject,
     period,
     lst_indep1,
     lst_indep2,
     lst_geobrand, 
     lst_geobrandsub,
     use_log1,
     use_mc1,
     file = paste0(outdir, '/model2.rda'))

etime = as.POSIXct(Sys.time())
cat( '\n', paste( 'Constrained Model took', round( difftime( etime, stime, units = "mins"), 2), 'minutes' ), '\n' )
