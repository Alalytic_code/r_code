##############################################################################
# Module: Linear Mixed-Effects Model (LME)                                   #
# Code: Source                                                               #
# Objective: Capture user inputs, run module scripts                         #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

#load generic packages
suppressWarnings(suppressMessages(library(dplyr)))
suppressWarnings(suppressMessages(library(scales)))
suppressWarnings(suppressMessages(library(ggplot2)))
suppressWarnings(suppressMessages(library(reshape2)))
suppressWarnings(suppressMessages(library(plotly)))
suppressWarnings(suppressMessages(library(jsonlite)))



script_path=dirname(parent.frame(2)$ofile)

setwd(script_path)


options(warn = -1)
output_stream=list()
#plot_stream = list()
#model_stream= list()




run_step <-function(outdir,step_name, ...){
  listVarname <- list(...)
  
  #save messsage and errors to output directory
  msg = file(paste0(outdir, '/',step_name , '_message',  '.log'), open='wt')
  #out = file(paste0(outdir, '/',step_name , '_output', '.log'), open='wt')
  
  
  sink(msg, type=c('output', 'message'), append=F)
  #sink(out, type=, append=F)
  # Assign ellipsis as local variable
  varnames=names(listVarname)
  assign('outdir',outdir)
  for(varname in varnames){
    assign(varname,listVarname[[varname]])
  }
  
  
  #load generic functions
  source('funlib.R', local = TRUE, verbose = FALSE, echo = FALSE)
  
  
  
  #### Load the step source
  step_source=paste(step_name,'.r',sep = "")
  source(step_source,local = TRUE)
  #output_stream(model_stream,plot_stream)
  
  
  
  cat("\nInput Arguments:", toJSON(listVarname, auto_unbox = TRUE))
  
  # add logs to 
  info = list(type="console", name="console", path= paste0(step_name, '_message.log'))
  len = length(output_stream)
  output_stream[[len+1]] <<- info
  
  
  #release sink file
  closeAllConnections()
  for(i in seq_len(sink.number())){
    sink(NULL)
  }
  gc()
  
  return(output_stream)
}
