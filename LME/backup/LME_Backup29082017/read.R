##############################################################################
# Module: Linear Mixed-Effects Model (LME)                                   #
# Code: read                                                                 #
# Objective: Read input data, print basic diagnostics                        #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################


stime = as.POSIXct(Sys.time())

suppressWarnings(suppressMessages(library(data.table))) #fread

cat('\n', '==================================================')
cat('\n', '          ***** CONSOLE OUTPUT: READ ***** ')
cat('\n', '==================================================', '\n')

#read input file
rdata = fread(paste0(outdir, '/', datafile), data.table = FALSE)

#rename key columns
names_old = c(depvar, subject, period, geobrand)
names_new = c('depvarcol', 'subjectcol', 'periodcol', 'geobrandcol')
for (i in 1:length(names_old)) {
  names(rdata)[which(names(rdata) == names_old[i])] = names_new[i]
}

#column containing market sales, if provided
if (mktsales == '' | mktsales == 'NA') mktsales1 = NULL else mktsales1 = mktsales

#create list of geobrand and indep vars
lst_geobrand = sort(unique(rdata$geobrandcol))
lst_indep1 = c(str_to_lst(lst_indep), mktsales1)


#convert character variables to factors
lst_fac = lst_indep1[which(sapply(rdata[, lst_indep1], is.character))]
if (length(lst_fac) > 0) {
  rdata = mutate_each_(rdata, funs(as.numeric(as.factor(.))), lst_fac)
  cat( '\n', 'NOTE: below character variables were converted to factors', '\n', lst_fac)
}



#get rid of useless columns
lst_indep1 = setdiff(lst_indep1, lst_fac)
rdata = rdata[names(rdata) %in% c(names_new, lst_indep1, lst_fac)]

#get start and end dates
pbounds1 = str_to_lst(pbounds)
pstart = as.Date(pbounds1[1])
pend = as.Date(pbounds1[2])

#print user selections
cat( '\n', paste0( 'input data file: ', datafile), '\n')
cat( '\n', paste0( nrow(rdata), ' records read from file'))
cat( '\n', paste0( 'period: ', period))
cat( '\n', paste0( 'start ', period, ': ', pstart))
cat( '\n', paste0( 'end ', period, ': ', pend), '\n')

#apply period filter
rdata$periodcol = as.Date(rdata$periodcol, datefmt)
rdata = rdata[rdata$periodcol >= pstart & rdata$periodcol <= pend, ]
if (nrow(rdata) == 0) stop('Period variable not in selected format ', datefmt) else 
                      cat( '\n', paste( nrow(rdata), 'records between', pstart, 'and', pend), '\n')

#missing value imputation - replace NA with zero (numeric columns only)
if (as.integer(impute) == 1) rdata = mutate_if(rdata, is.numeric, funs(ifelse(is.na(.), 0, .)))

#outlier removal based on std dev of depvar
if (outlier > 0) {
  outlier1 = as.numeric(outlier)
  summ = group_by(rdata, geobrandcol) %>% summarise_each(funs(mean, sd), depvarcol) %>% mutate(ul = mean + outlier1*sd)
  rdata = merge(rdata, summ, by='geobrandcol')
  cat('\n', paste('Outlier removal: No. of dropped records by', geobrand))
  print(table(rdata[rdata$depvarcol > rdata$ul, ]$geobrandcol))
  rdata = rdata[rdata$depvarcol <= rdata$ul, ]
  rdata = rdata[!names(rdata) %in% c('mean', 'sd', 'ul')]
}

if (!costfile == 'None') {
  #validate cost file
  costfile1 = paste0(outdir, '/', costfile)
  cols = header(costfile1)
  gbchk = uniq(costfile1, cols[1])  #geobrands present in cost file
  varchk = uniq(costfile1, cols[2]) #variables present in cost file
  if (!all(tolower(lst_geobrand) %in% tolower(gbchk))) stop('Missing Brands/Geographies in cost file')
  if (!all(tolower(lst_indep1) %in% tolower(varchk))) stop('Missing Variables in cost file')

  #read cost file
  unit_cost = fread(costfile1, data.table = FALSE )
  names(unit_cost) = c('geobrand', 'channel', 'unit_cost')
  unit_cost = filter(unit_cost, geobrand %in% lst_geobrand, channel %in% lst_indep1) %>% arrange(geobrand, channel)

} else {
  unit_cost = expand.grid(lst_geobrand, lst_indep1, 1)
  names(unit_cost) = c('geobrand', 'channel', 'unit_cost')
}

#unit cost for optimization (nested list)
lst_cost = mlist(lst_geobrand)
for (gb in lst_geobrand) {
  lst_cost[[gb]] = unit_cost[unit_cost$geobrand == gb, ]$unit_cost
  names(lst_cost[[gb]]) = lst_indep1  
}

#total activity by geobrand-channel
activity = group_by(rdata, geobrandcol) %>% summarise_each_(funs(sum(.)), lst_indep1)
activity = melt(activity, id.vars = 'geobrandcol')
names(activity) = c('geobrand', 'channel', 'activity')
activity = filter(activity, geobrand %in% lst_geobrand) %>% arrange(geobrand, channel)

#total cost for ROI calculation (nested list)
tbl_cost = merge(activity, unit_cost, by=c('geobrand', 'channel'))
tbl_cost$spend = tbl_cost$activity * tbl_cost$unit_cost
rownames(tbl_cost) = NULL #for printing
total_cost = mlist(lst_geobrand)
for (gb in lst_geobrand) {
  total_cost[[gb]] = tbl_cost[tbl_cost$geobrand == gb, ]$spend
  names(total_cost[[gb]]) = lst_indep1
}

#print spend summary by geobrand
for (gb in lst_geobrand) {
  cat('\n', paste('----- Channel Spend Summary for Geo/Brand', gb, '-----'), '\n')
  print(tbl_cost[tbl_cost$geobrand == gb, ])
}

#histogram for depvar and each indep var 
hist = function(var) {
  x = rdata[[var]] #avoid subsetting within aes stmt
  plotly_hist=ggplot(rdata, aes(x = x, fill = geobrandcol)) + 
  #facet_wrap(~geobrandcol, scales = "free") + 
  geom_histogram(bins = 5, color = 'Black', size = 0.2) +
  ggtitle(paste('Histogram for', var)) +
  theme(plot.title = element_text(hjust=0.5, size=18, face='bold'),
        axis.text.x = element_text(size=8, colour="black"),
        axis.text.y = element_text(size=8, colour="black"),
        strip.text.x = element_text(size=18),
        axis.title.x = element_blank(),
        legend.position = "none") +
  labs(y = '# Records')
  title=paste('Histogram for', var)
  xAxis=list(label="# of Records")
  yAxis=list(label=var)
  plot_to_json(var,plotly_hist,xAxis=xAxis,yAxis=yAxis,title=title,write_file = TRUE)
  #return(plotly_hist)
}
rplot1 = lapply(c('depvarcol', lst_indep1), hist)

#plot number of records by period
rplot2 = 
data.frame(table(rdata$geobrandcol, rdata$periodcol)) %>%
  mutate(geobrandcol=Var1, periodcol=Var2, No_of_records=Freq) %>%
  ggplot(aes(x=periodcol, y=No_of_records, fill=geobrandcol)) +
  facet_wrap(~geobrandcol, scales = "free", ncol=2) +
  geom_bar(stat = "identity", width = 0.8, colour = 'Black', size = 0.2) + 
  ggtitle(paste('No. of records by', period)) +
  theme(plot.title = element_text(hjust=0.5, size=18, face='bold'), 
        axis.text.x=element_blank(), axis.ticks.x=element_blank(),
        axis.text.y = element_text(size=8, colour="black"),
        strip.text.x = element_text(size=18),
        axis.title.x = element_blank(),
        legend.position = "none", 
        panel.border = element_rect(colour="black", fill=NA, size=1)) +
  labs(y = '# Records')+geom_text(aes(y=0, label=periodcol), angle=90, size=4, hjust=0) +
  geom_text(aes(y = 0, label = periodcol), angle=90, size=4, hjust=0)
#title=paste('No. of records by', period)
#xAxis=list(label="period",tickFormat="mm/dd/YYYY")
#yAxis=list(label=var)
#plot_to_json(Freq,rplot2,xAxis=xAxis,yAxis=yAxis,title=title,write_file = TRUE )

#apply subject inclusion threshold ('static')
cat('\n', paste0('No. of observations by ', geobrand, ' (BEFORE applying subject inclusion threshold)'))
print(table(rdata$geobrandcol))

static1 = floor((as.numeric(static)/100) * length(unique(rdata$periodcol)))
n1 = length(unique(rdata$subjectcol))                                                 #before applying static
staticPlot = filter(data.frame(table(rdata$geobrandcol, rdata$subjectcol)), Freq > 0) #for charting
staticData = filter(staticPlot, Freq >= static1)                                      #for applying static filter
names(staticPlot) = names(staticData) = c('geobrandcol', 'subjectcol', 'n_by_sub')
rdata = merge(rdata, staticData, by=c('geobrandcol', 'subjectcol'))
n2 = length(unique(rdata$subjectcol))                                                 #after applying static
if (n1 > n2) cat('\n', paste( 'NOTE: dropped', n1-n2, subject, 'not meeting', static1, period, 'static'), '\n')

cat('\n', paste0('No. of observations by ', geobrand, ' (AFTER applying subject inclusion threshold)'))
print( table(rdata$geobrandcol) )
rdata = rdata[!names(rdata) == 'n_by_sub']

#plot number of records by subject (static plot)
rplot3 =
  ggplot(staticPlot, aes(x=n_by_sub, fill=n_by_sub >= static1)) + 
  geom_bar(colour = 'Black', size = 0.2) + 
  facet_wrap(~geobrandcol, scales = "free") +
  labs(x = paste0('# ', period, 's of data'), y = paste0('Number of ', subject)) +
  guides(fill = FALSE) + 
  ggtitle(paste('No. of excluded', subject, 'by', geobrand)) + 
  theme(plot.title = element_text(hjust=0.5, size=18, face='bold'),
        axis.text.x = element_text(size=8, colour="black"),
        axis.text.y = element_text(size=8, colour="black"),
        strip.text.x = element_text(size=18),
        panel.border = element_rect(colour="black", fill=NA, size=1))
title=paste('No. of excluded', subject, 'by', geobrand)
xAxis=list(label=paste0('# ', period, 's of data'))
yAxis=list(label=paste0('Number of ', subject))
plot_to_json('no_of_Excludes',rplot3,xAxis=xAxis,yAxis=yAxis,title=title,write_file = TRUE )

#export plots to HTML (comment in test mode)
#plot_to_html(rplot1, 'histogram')
#plot_to_html(rplot2, 'records_by_period')
#plot_to_html(rplot3, 'excluded_subjects')

#export plots to HTML (comment in test mode)
#suppressWarnings(plot_to_json('histogram', rplot1, write_file = TRUE,save_output = TRUE))
#plot_to_json('records_by_period', rplot, write_file = TRUE,save_output = TRUE2)
#plot_to_json('excluded_subjects', rplot3, write_file = TRUE,save_output = TRUE)

#Exit Read
save(rdata, 
     lst_indep1,
     lst_fac,
     depvar, 
     subject, 
     period, 
     geobrand, 
     lst_geobrand, 
     datefmt, 
     mktsales1,
     total_cost,
     lst_cost,
     file = paste0(outdir, '/read.rda'))
etime = as.POSIXct(Sys.time())
cat('\n', paste( 'Read took', round( difftime( etime, stime, units = 'mins'), 2), 'minutes' ), '\n')
