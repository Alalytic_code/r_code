##############################################################################
# Module: Channel Attributor                                                 #
# Code: read.R                                                               #
# Objective: Read input data, print basic diagnostics                        #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

stime = as.POSIXct(Sys.time())

suppressWarnings(suppressMessages(library(data.table))) #fread

########################################### Start Definitions ########################################
#generates histogram data for one gb and one var
histfun1 = function(gb, var) {
  x = rdata[rdata$geobrandcol==gb, var]
  #check whether all values are same (no histogram possible)
  if (sd(x) > 0) {
    bin_width = 3.5 * sd(x) * length(x) ^ (-1/3)
    breaks = ceiling((max(x) - min(x)) / bin_width)
    objhist = hist(x, breaks = breaks, plot = FALSE) #suppress plotting of histogram
    histdata = data.frame(x=objhist$mids, y=objhist$counts)
  } else histdata = NULL
  
  return(histdata)
}

#generates histogram data for one var across all gb
histfun2 = function(var) {
  histdata = lapply(lst_geobrand, histfun1, var)
  histdf = data.frame(x=numeric(0), y=numeric(0), group=character(0)) 
  for (i in 1:length(lst_geobrand)) {
    foo = histdata[[i]]
    if (!is.null(foo)) {
      foo$group = lst_geobrand[i]
      histdf = rbind(histdf, foo)
    }
  }

  var = ifelse(var == 'depvarcol', depvar, var) #proper label for depvar
  
  #export to JSON
  json_toPlot(plot_name = var,
              category = 'Histogram_Plot',
              plot_object = list(data = histdf,
                                 xAxis = list(label=var),
                                 yAxis = list(label='# Records'),
                                 viewMode = 'panel',
                                 title = paste('Histogram for', var),
                                 chartType = 'bar',
                                 mode = 'stack'),
                            write_file=FALSE)
  return(histdf)
}
########################################### End Definitions ########################################

cat('\n', '==================================================')
cat('\n', '          ***** DATA READ ***** ')
cat('\n', '==================================================', '\n')

#read input file
rdata = fread(paste0(outdir, '/', datafile), data.table = FALSE)

#rename key columns
names_old = c(depvar, subject, period, geobrand)
names_new = c('depvarcol', 'subjectcol', 'periodcol', 'geobrandcol')
for (i in 1:length(names_old)) {
  names(rdata)[which(names(rdata) == names_old[i])] = names_new[i]
}

#create list of geobrand and indep vars
lst_geobrand = sort(unique(rdata$geobrandcol))
lst_indep1 = c(str_to_lst(lst_indep), mktsales)

#get start and end dates
pbounds1 = str_to_lst(pbounds)
pstart = as.Date(pbounds1[1], datefmt)
pend = as.Date(pbounds1[2], datefmt)

#print user selections
cat( '\n', paste0( 'input data file: ', datafile), '\n')
cat( paste(nrow(rdata), 'records read from data file'), '\n')
cat( '\n', paste0( 'period: ', period))
cat( '\n', paste0( 'start ', period, ': ', pstart))
cat( '\n', paste0( 'end ', period, ': ', pend), '\n')

#convert character variables to factors
lst_fac = lst_indep1[which(sapply(rdata[, lst_indep1], is.character))]
if (length(lst_fac) > 0) {
  rdata = mutate_each_(rdata, funs(as.numeric(as.factor(.))), lst_fac)
  cat( '\n', 'NOTE: below character variables were converted to factors', '\n', lst_fac)
}

lst_indep1 = setdiff(lst_indep1, c(lst_fac, mktsales))
rdata = rdata[names(rdata) %in% c(names_new, lst_indep1, lst_fac, mktsales)]

#apply period filter
rdata$periodcol = as.Date(rdata$periodcol, datefmt)
rdata = rdata[rdata$periodcol >= pstart & rdata$periodcol <= pend, ]
if (nrow(rdata) == 0) stop('Period variable not in selected format ', datefmt) else 
                      cat( '\n', paste( nrow(rdata), 'records between', pstart, 'and', pend), '\n')

#missing value imputation - replace NA with zero (numeric columns only)
if (as.integer(impute) == 1) rdata = mutate_if(rdata, is.numeric, funs(ifelse(is.na(.), 0, .)))

#outlier removal based on std dev of depvar
if (outlier > 0) {
  outlier1 = as.numeric(outlier)
  summ = group_by(rdata, geobrandcol) %>% summarise_each(funs(mean, sd), depvarcol) %>% mutate(ul = mean + outlier1*sd)
  rdata = merge(rdata, summ, by='geobrandcol')
  
  #print table of # of deleted records per geobrand
  foo = table(rdata[rdata$depvarcol > rdata$ul, ]$geobrandcol)
  if (nrow(foo) > 0) {
    cat('\n', paste('Outlier removal: No. of dropped records by', geobrand))
    print(foo)
  }
  rdata = rdata[rdata$depvarcol <= rdata$ul, ]
  rdata = dplyr::select(rdata, -mean, -sd, -ul)
}

#total activity by geobrand-channel
activity = group_by(rdata, geobrandcol) %>% summarise_each_(funs(sum(.)), lst_indep1)
activity = melt(activity, id.vars = 'geobrandcol')
names(activity) = c('geobrand', 'channel', 'activity')
activity$geobrand = tolower(activity$geobrand) #to avoid mismatch of geobrand with cost file
activity$channel = tolower(activity$channel)   #to avoid mismatch of channel names with cost file
activity = filter(activity, geobrand %in% tolower(lst_geobrand))

if (!is.null(costfile)) {
  #validate cost file
  costfile1 = paste0(outdir, '/', costfile)
  cols = header(costfile1)
  gbchk = tolower(uniq(costfile1, cols[1]))   #geobrands present in cost file
  varchk = tolower(uniq(costfile1, cols[2]))  #variables present in cost file
  
  if (!all(tolower(lst_geobrand) %in% gbchk)) {
    msg = paste('Missing', geobrand, 'in cost file. Please ensure', geobrand, 'names in cost file match with data file')
    cat(paste(geobrand, 'found in cost file:'), paste(gbchk, collapse = ', '), '\n')
    cat(paste(geobrand, 'found in data file:'), paste(tolower(lst_geobrand), collapse = ', '), '\n')
    stop(msg)
  }
  
  if (!all(tolower(lst_indep1) %in% varchk)) {
    msg = 'Missing variables in cost file. Please ensure variable names in cost file match with data file.'
    cat(paste(' Variables found in cost file:'), paste(varchk, collapse = ', '), '\n')
    cat(paste(' Variables found in data file:'), paste(tolower(lst_indep), collapse = ', '), '\n')
    stop(msg)
  }

  #read cost file
  costdata = fread(costfile1, data.table = FALSE )
  cat('\n', paste(nrow(costdata), 'records read from cost file'), '\n')
  names(costdata) = c('geobrand', 'channel', 'total_cost')
  foo = paste(setdiff(varchk, tolower(lst_indep1)), collapse = ', ')
  if (nchar(foo) > 0) cat(paste('Extra variables in cost file were ignored:', foo), '\n')
  
  costdata$geobrand = tolower(costdata$geobrand)
  costdata$channel = tolower(costdata$channel)
  costdata = filter(costdata, geobrand %in% tolower(lst_geobrand), channel %in% tolower(lst_indep1))
  
  #calculate unit cost = total cost/activity
  costdata = merge(activity, costdata, by = c('geobrand', 'channel'))
  costdata$unit_cost = round( ifelse(costdata$activity>0, costdata$total_cost/costdata$activity, 0), 4 )

} else {
  #if no cost file provided assume per unit cost of 1 for all channels
  costdata = expand.grid(tolower(lst_geobrand), tolower(lst_indep1), NA)
  names(costdata) = c('geobrand', 'channel', 'total_cost')
  costdata = merge(activity, costdata, by = c('geobrand', 'channel'))
  costdata$total_cost = costdata$activity
  costdata$unit_cost = 1
  cat('\n', 'NOTE: No cost information provided. ROI calculation will use same cost for all channels.', '\n')
}

#sort costdata to maintain same order as data file
sort1 = data.frame(geobrand = tolower(lst_geobrand), sort1 = seq(length(lst_geobrand)))
sort2 = data.frame(channel = tolower(lst_indep1), sort2 = seq(length(lst_indep1)))
costdata = merge( merge(costdata, sort2, by='channel'), sort1, by='geobrand' )
costdata = arrange(costdata, sort1, sort2)
costdata = dplyr::select(costdata, -sort1, -sort2)

#total cost required for ROI calculation | unit cost required for Optimization
total_cost = unit_cost = mlist(unique(costdata$geobrand))
for (gb in tolower(lst_geobrand)) {
  foo = costdata[costdata$geobrand == gb, ]
  total_cost[[gb]] = foo$total_cost
  names(total_cost[[gb]]) = foo$channel
  unit_cost[[gb]] = foo$unit_cost
  names(unit_cost[[gb]]) = foo$channel
}

#print spend summary by geobrand
names(costdata)[1] = geobrand
for (gb in tolower(lst_geobrand)) {
  cat('\n', paste('----- Channel Spend Summary for', geobrand, gb, '-----'), '\n')
  print(costdata[costdata[[geobrand]] == gb, ], row.names = FALSE)
}

#generate histogram data and export to JSON
plotdata = lapply(c('depvarcol', lst_indep1), histfun2)

#plot number of records by period and export to JSON
plotdata = 
  data.frame(table(rdata$geobrandcol, as.numeric(as.POSIXct(rdata$periodcol)))) %>% 
    rename(x=Var2, y=Freq, group=Var1) %>%
      arrange(group, x)

#dynamic date format for week vs month
date_interval = as.integer(rdata$periodcol[2] - rdata$periodcol[1])
if (date_interval <= 7) tickFormat = '%b-%d-%Y' else  tickFormat = '%b-%Y'

#export to JSON
json_toPlot(plot_name = 'Records_By_Period',
            category = 'Records_By_Period',
            plot_object = list(data = plotdata,
                               xAxis = list(label=period, type='date', tickFormat=tickFormat),
                               yAxis = list(label='# Records'),
                               viewMode = 'panel',
                               title = paste('No. of records by', period),
                               chartType = 'bar',
                               mode = 'stack'),
                          write_file=FALSE)

#apply subject inclusion threshold ('static')
cat('\n', paste0('No. of observations by ', geobrand))
print(table(rdata$geobrandcol))

static1 = floor((as.numeric(static)/100) * length(unique(rdata$periodcol)))
n1 = length(unique(rdata$subjectcol))                                                 #before applying static
staticPlot = filter(data.frame(table(rdata$geobrandcol, rdata$subjectcol)), Freq > 0) #for charting
staticData = filter(staticPlot, Freq >= static1)                                      #for applying static filter
names(staticPlot) = names(staticData) = c('geobrandcol', 'subjectcol', 'n_by_sub')
rdata = merge(rdata, staticData, by=c('geobrandcol', 'subjectcol'))
n2 = length(unique(rdata$subjectcol))                                                 #after applying static

if (n1 > n2) {
  cat('\n', paste( 'NOTE: dropped', n1-n2, subject, 'not meeting', static1, period, 'static'), '\n')
  cat('\n', paste0('No. of observations by ', geobrand, ' (AFTER applying subject inclusion threshold)'))
  print( table(rdata$geobrandcol) )
} else cat('\n', 'NOTE: All subjects meet inclusion criteria, none removed', '\n')

rdata = dplyr::select(rdata, -n_by_sub)

#plot showing no. of excluded subjects by geobrand (static-plot)
staticPlot$colour = ifelse(staticPlot$n_by_sub >= static1, '#7FB800', '#FF5733')
plotdata = group_by(staticPlot, geobrandcol, n_by_sub, colour) %>% 
         summarise(n=n()) %>% 
         rename(group=geobrandcol, x=n_by_sub, y=n)

if (length(unique(plotdata$colour)) > 1) {
  #export to JSON
  json_toPlot(plot_name = paste0('Excluded Subjects'),
              category = 'Excluded Subjects',
              plot_object = list(data = plotdata,
                                 xAxis = list(label=paste(period, 'of data')),
                                 yAxis = list(label=paste('No. of', subject)),
                                 viewMode = 'panel',
                                 title = paste('No. of excluded subjects by', geobrand),
                                 chartType = 'bar'),
                            write_file=FALSE)
}

#Exit Read
save(rdata, 
     lst_indep1,
     lst_fac,
     depvar, 
     subject, 
     period, 
     geobrand, 
     lst_geobrand, 
     datefmt, 
     mktsales,
     total_cost,
     unit_cost,
     tickFormat,
     file = paste0(outdir, '/read.rda'))

etime = as.POSIXct(Sys.time())
cat('\n', paste( 'Data Read took', round( difftime( etime, stime, units = 'mins'), 2), 'minutes' ), '\n')
