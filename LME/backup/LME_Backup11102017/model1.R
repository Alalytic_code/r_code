##############################################################################
# Module: Channel Attributor                                                 #
# Code: model1.R                                                             #
# Objective: Run LMER model for various options, select best model data      #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

stime = as.POSIXct(Sys.time())

suppressWarnings(suppressMessages(library(lme4)))          #lmer 
suppressWarnings(suppressMessages(library(car)))           #vif 
suppressWarnings(suppressMessages(library(ppcor)))         #pcor 
suppressWarnings(suppressMessages(library(DT)))            #datatable

source(file = 'funmod.R', verbose = FALSE, echo = FALSE)

########################################### Section 1: Definitions ########################################
#variance inflation factors (run on untransformed variables)
runVIF = function(gb) { 
  vifdata = pdata[pdata$geobrandcol == gb, ]  #filter for geobrand
  form_lm = as.formula( paste('depvarcol', paste(lst_indep2[[gb]], collapse = '+'), sep = '~') )
  VIF = vif(lm(formula = form_lm, data = vifdata))
  VIF = data.frame(x = names(VIF), 
                   y = VIF, 
                   colour = ifelse(VIF >= 5, '#FF5733', '#006400'), 
                   row.names=NULL)
  
  #export to JSON
  json_toPlot(plot_name = toupper(gb),
              category = 'VIF_Plot',
              plot_object = list(data = VIF,
                                 xAxis = list(label='Variables'),
                                 yAxis = list(label='VIF'),
                                 viewMode = 'carousel',
                                 title = paste0('Variance Inflation Factors for ', toupper(gb)),
                                 name = paste0('VIF_Plot ', gb),
                                 chartType = 'bar'),
                            write_file=FALSE)
  return(VIF)
}

#partial correlation
pCorrFun = function(gb) {
  pcor1 = round((pcor(pdata[pdata$geobrandcol==gb, lst_indep2[[gb]]], method = 'pearson')$estimate), 4)
  pcor1[upper.tri(pcor1)] = NA
  pcor1 = melt(pcor1, na.rm = T)
  pcor1 = filter(pcor1, !Var1==Var2)
  names(pcor1) = c('x', 'y', 'z')

  #export to JSON
  json_toPlot(plot_name = toupper(gb),
              category = 'Partial_correlation',
              plot_object = list(data = pcor1,
                                 xAxis = list(label=NULL),
                                 yAxis = list(label=NULL),
                                 viewMode = 'carousel',
                                 title = paste0('Partial Correlation Matrix for ', gb),
                                 chartType = 'heatmap'),
                            write_file=FALSE)
  return(pcor1)
}

#run LMER mixed model for given covariance structure 
runLMER = function() {
  #create model formula based on covariance structure option (|| implies uncorrelated random slopes)
  form_fxd = paste('depvarcol', paste0(c(lst_fxdvars, trend1, seas, compt, lst_fac), collapse = ' + '), sep = ' ~ ')
  form_rnd = ifelse(iCovStr==2, paste0('(0 + ', paste(lst_rndvars, collapse = ' + '), ' | ',  'subjectcol', ')' ),
                                paste0('(0 + ', paste(lst_rndvars, collapse = ' + '), ' || ', 'subjectcol', ')' ) )
  formula = as.formula(paste(form_fxd, '+', form_rnd))
  
  #run LMER
  modobj = lmer(formula = formula, data = mdata, control = lmerControl(calc.derivs = FALSE))
  return(modobj)
}

#extract fixed effect estimates from a LMER object
fxdLMER = function(model) {
  fixed = coef(summary(model))[, 1][lst_fxdvars]
  fixed[fixed < 0] = 0 #set negative estimates to zero
  names(fixed) = paste0('fxd_', lst_fxdvars)
  return(fixed)
}

#extract random effect estimates from a LMER object
rndLMER = function(model) {
  rand = coef(model)$subjectcol[, lst_rndvars]
  rand[rand < 0] = 0 #set negative estimates to zero
  rand = cbind(rownames(rand), rand)
  rownames(rand) = NULL
  names(rand) = c('subjectcol', paste0('rnd_', lst_rndvars))
  return(rand)
}

########################################### Section 2: LMER Setup ####################################################
load(file = paste0(outdir, '/prep.rda'))

cat("\n", '============================================================')
cat("\n", '          ***** UNCONSTRAINED MIXED MODEL ***** ')
cat("\n", '============================================================', "\n")

#sort model data by geobrand, subject and period
pdata = arrange(pdata, geobrandcol, subjectcol, periodcol)

#list of unique geobrand values - ALL geobrands if use_geobrand is blank
if (!is.null(use_geobrand)) lst_geobrand = str_to_lst(use_geobrand)

#subjects for each geobrand (nested list)
lst_geobrandsub = mlist(lst_geobrand)
for (gb in lst_geobrand) {
  lst_geobrandsub[[gb]] = unique(pdata[pdata$geobrandcol==gb, 'subjectcol'])
}

#set randomized variables to ALL if lst_rand is blank (applies across geobrands)
if (is.null(lst_rand)) lst_rand1 = lst_indep1 else lst_rand1 = str_to_lst(lst_rand)

#modify list of model vars to exclude zero sd vars
lst_indep2 = lst_rand2 = mlist(lst_geobrand)
for (gb in lst_geobrand) {
  remove_vars = zero_sd_vars(pdata[pdata$geobrandcol == gb, lst_indep1])
  lst_indep2[[gb]] = setdiff(lst_indep1, remove_vars)
  lst_rand2[[gb]] = intersect(lst_rand1, lst_indep2[[gb]])
}

#same as above for cost
total_cost1 = mlist(tolower(lst_geobrand))
for (gb in lst_geobrand) total_cost1[[tolower(gb)]] = total_cost[[tolower(gb)]][tolower(lst_indep2[[gb]])]

#drop the periods and/or subjects specified by user (applies across geobrands)
#if both period and subject entered, drop combination of subject and period
if (!is.null(drop_subject) & is.null(drop_period)) {
  pdata = pdata[!pdata$subjectcol %in% str_to_lst(drop_subject), ]
  cat('\n', paste('NOTE:', subject, drop_subject, 'excluded from analysis'), '\n')
  
} else if (!is.null(drop_period) & is.null(drop_subject)) {
  pdata = pdata[!pdata$periodcol %in% as.Date(str_to_lst(drop_period), datefmt), ]
  cat('\n', paste('NOTE:', period, drop_period, 'excluded from analysis'), '\n')
  
} else if (!is.null(drop_subject) & !is.null(drop_period)) {
  pdata = pdata[!pdata$subjectcol %in% str_to_lst(drop_subject) | 
                !pdata$periodcol %in% as.Date(str_to_lst(drop_period), datefmt), ]
  cat('\n', paste0('NOTE: ', 'Observations where ', subject, ' in (', drop_subject, 
                   ') AND period in (', drop_period, ') excluded from analysis'), '\n')
}

#set covariance structure options
#1=uncorrelated random slopes | 2=correlated random slopes
covstr = 1:2
covstrlabel = c('Uncorrelated Random Effects', 'Correlated Random Effects')

#set trend and seas options
trendseas = 1:4
trendseaslabel = c('Dummy Seasonality With Trend', 
                   'Dummy Seasonality Without Trend',
                   ifelse(is.null(mktsales), 'Without Seasonality With Trend', 'Seasonality Factor With Trend'), 
                   ifelse(is.null(mktsales), 'Without Seasonality Without Trend', 'Seasonality Factor Without Trend'))

#margin for each geobrand (=1 if not provided)
if (is.null(lst_margin)) lst_margin1 = rep(1, length(lst_geobrand)) else 
                         lst_margin1 = as.numeric(str_to_lst(lst_margin))
names(lst_margin1) = lst_geobrand

#print basic info
cat('\n', paste0('No. of ', geobrand, ': ', length(lst_geobrand)))
cat('\n', paste0('No. of ', period, ': ', length(unique(pdata$periodcol))), '\n')
cat('\n', paste(length(lst_indep1), 'channels modeled:'), paste(lst_indep1, collapse = ', '))
cat('\n', paste0(length(lst_rand1), ' channels randomized by ', subject, ': ', paste(lst_rand1, collapse = ', ')), '\n')

#variance inflation factors (JSON for bar chart)
plotdata = lapply(lst_geobrand, runVIF)

#partial correlation matrix (JSON for heatmap)
plotdata = lapply(lst_geobrand, pCorrFun)

#list of models to be run for each geobrand
seq_model = seq(1, length(lst_lambda)*length(covstr)*length(trendseas))
lst_model = paste0('model', formatC(seq_model, width = 2, format = "d", flag = "0"))
cat('\n', paste0(length(seq_model), ' models run for each ', geobrand, 
                 '. Best model data selected basis lowest AIC.'), '\n')

#initialize DFs to store model parameters and fit stats
PARAMS = data.frame( matrix(NA, ncol = 5, nrow = length(lst_geobrand)*length(lst_model))) 
names(PARAMS) = c('geobrand', 'Model', 'Lambda', 'CovStr', 'TrendSeas')
FitStats = data.frame( matrix(NA, ncol = 4, nrow = length(lst_geobrand)*length(lst_model))) 
names(FitStats) = c('geobrand', 'Model', 'R2', 'AIC')
FitStats[c(1,2)] = PARAMS[c(1,2)] = arrange(expand.grid(lst_geobrand, seq_model), Var1)

#initialize DFs to store contribs, ROIs and estimates
CONTRIB = ROI = FIXED = data.frame( matrix( NA, ncol=4, nrow=0))
names(CONTRIB) = names(FIXED) = names(ROI) = c('geobrand', 'variable', 'model', 'value')

############################################ Section 3: LMER Run ##################################################
for (iGeoBrand in lst_geobrand) {
  iModel = 1
  for (iLambda in lst_lambda) {
    for (iCovStr in covstr) {
      for (iTrendSeas in trendseas) {
        
        gc() #Garbage Collection
        
        #set, store and print model parameters
        setParams(gb = iGeoBrand, 
                  dk = iLambda, 
                  ts = iTrendSeas, 
                  fxdvars = lst_indep2[[iGeoBrand]], 
                  rndvars = lst_rand2[[iGeoBrand]])
        PARAMS[PARAMS$geobrand==iGeoBrand & PARAMS$Model==iModel, -c(1,2)] = c(iLambda, iCovStr, iTrendSeas)
        dklabel = ifelse(dk == '', '', paste('Decay Rate', paste0(dk, '0')))
        cat('\n', paste0('----- Model #', iModel, ' for ', geobrand, ' ', iGeoBrand, ': ', 
                         dklabel, ' | ', covstrlabel[iCovStr], ' | ', trendseaslabel[iTrendSeas], ' -----'), '\n')

        #run LMER
        modLMER = runLMER()
        print(summary(modLMER)$coefficients)
        pred = fitted(modLMER)
        
        #temp df to store estimates, contribs and ROIs
        fixed = fxdLMER(modLMER)
        tempdf = data.frame(geobrand=iGeoBrand, 
                            variable=lst_indep2[[iGeoBrand]], 
                            model=lst_model[iModel],
                            value=fixed, 
                            row.names=NULL)
        FIXED = rbind(FIXED, tempdf)

        #Overall decomps
        keepvars = c('depvarcol', lst_fxdvars, names(fixed), mean_y, lst_mean_fxd)
        data1 = cbind(mdata, t(fixed))[, keepvars]
        resDecomp = decomp(df=data1, xcols=lst_fxdvars, ecols=names(fixed), meanx=lst_mean_fxd)
        
        #incremental units
        incrLMER = colSums(resDecomp[, lst_fxdvars]) 
        contribLMER = round(100 * (incrLMER / sum(untransform(pred))), 2) #channel pct contribution
        tempdf$value = contribLMER
        CONTRIB = rbind(CONTRIB, tempdf)
        
        #channel ROI
        roiLMER = getROI(incr=incrLMER, margin=lst_margin1[iGeoBrand], spend=total_cost1[[tolower(iGeoBrand)]])
        tempdf$value = roiLMER
        ROI = rbind(ROI, tempdf)
        
        #print contribs and ROI
        cat( '\n', '----- Channel Contributions -----', '\n' ) 
        print(contribLMER)
        cat( '\n', paste0('Total Incremental Units = ', round(sum(contribLMER), 2), '%' ), '\n' )
        cat( '\n', '----- Channel ROI -----', '\n' ) 
        print(roiLMER)

        #store and report fit stats
        R2 = round(RSquared(mdata$depvarcol, pred), 1)
        AIC = round(AIC(modLMER), 0)
        FitStats[FitStats$geobrand==iGeoBrand & FitStats$Model==iModel, -c(1,2)] = c(R2, AIC)
        cat( '\n', paste0( 'R-squared = ', R2, '%'))
        cat( '\n', paste0( 'AIC (smaller is better) = ', AIC ), '\n' )

        iModel = iModel + 1
        gc()
      }
    }
  }
}

############################################ Section 4: Model Selection #############################################
#choose best model for each geobrand based on least AIC
best_model = integer(length(lst_geobrand))
names(best_model) = lst_geobrand
best_est = mlist(lst_geobrand)
for (gb in lst_geobrand) {
  iFitStats = filter(FitStats, geobrand==gb)
  best_model[[gb]] = min(which(iFitStats$AIC==min(iFitStats$AIC))) #in case of tie take the earlier one
  foo = paste0('model', formatC(best_model[[gb]], width = 2, format = "d", flag = "0"))
  best_est[[gb]] = FIXED[FIXED$model == foo, 'value']
  names(best_est[[gb]]) = FIXED[FIXED$model == foo, 'variable']
}

#interactive HTML table showing complete LMER results
colnames(FIXED)[4] = 'estimate'; colnames(CONTRIB)[4] = 'contrib'; colnames(ROI)[4] = 'ROI'
AllResults = merge(merge(FIXED, CONTRIB, by = c('geobrand', 'variable', 'model')), 
                             ROI, by = c('geobrand', 'variable', 'model'))
resLMER = datatable_to_json("all_results", 
                            AllResults, 
                            category = "All Results", 
                            caption = 'All Model Results (without priors)',
                            options = list(columns = list("geobrand" , "model",  "variable" , "estimate", "contrib", "ROI")), 
                            write_file = TRUE)

#Exit Model1
save(pdata,
     geobrand,
     depvar, 
     subject, 
     period, 
     mktsales,
     lst_indep1,
     lst_indep2,
     lst_fac,
     lst_rand2, 
     lst_seas, 
     use_log1, 
     use_mc1, 
     lst_geobrand, 
     lst_geobrandsub, 
     total_cost1,
     unit_cost,
     lst_margin1, 
     best_model, 
     best_est,
     PARAMS, 
     tickFormat,
     file = paste0(outdir, '/model1.rda'))
if (exists('lst_mean')) append(lst_mean, paste0(outdir, '/model1.rda')) 

etime = as.POSIXct(Sys.time())
cat( '\n', paste( 'Unconstrained Model took', round( difftime( etime, stime, units = "mins"), 2), 'minutes' ), '\n' )
