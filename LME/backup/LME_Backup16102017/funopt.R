##############################################################################
# Objective: Define common functions used by SIMUL and OPTIM                 #
##############################################################################

#setup common variables for optim and simul
setParams = function() {
  nprd = length(unique(pdata$periodcol))                               #number of periods
  nsub = length(lst_geobrandsub[[gb]])                                 #number of subjects
  lst_indep3 = lst_indep2[[gb]]                                        #list of channels
  histx = apply(pdata[pdata$geobrandcol == gb, lst_indep3], 2, mean)   #historical activity levels
  unit_cost1 = unit_cost[[tolower(gb)]][tolower(lst_indep3)]           #per unit channel cost
  est = filter(fxdOpt, geobrand==gb) %>% .$value                       #named list of model estimates
  names(est) = filter(fxdOpt, geobrand==gb) %>% .$variable

  #copy to parent env
  assign('nprd', nprd , envir = local_env)
  assign('nsub', nsub , envir = local_env)
  assign('lst_indep3', lst_indep3, envir = local_env)
  assign('histx', histx , envir = local_env)
  assign('unit_cost1', unit_cost1 , envir = local_env)
  assign('est', est , envir = local_env)
}


#incremental units for a single channel
incr_fn1 = function(x, beta) {
  if (use_log1 == 0) {
    y = beta * x * nsub * nprd                         #linear model
  } else if (use_log1 == 1) {
    y = beta * logtr(x) * nsub * nprd                  #semi-log model
  } else {
    factor = exp(beta * logtr(x))                      #log-linear model
    y1 = sum(untransform(pdata[pdata$geobrandcol==gb, 'depvarcol']))
    y = y1 - y1/factor
  }
  return(y)
}

#channel cost for a single channel
cost_fn1 = function(x, cost) {
  return(x * cost * nsub * nprd)
}

#incremental units for iVar-th channel (hypothetical or historical)
incr_fn2 = function(iVar, type) {
  x = ifelse(type=='hist', histx[iVar], hypox[iVar])
  return(incr_fn1(x=x, beta=est[iVar]))
}

#cost for iVar-th channel (hypothetical or historical)
cost_fn2 = function(iVar, type) {
  x = ifelse(type=='hist', histx[iVar], hypox[iVar])
  return(cost_fn1(x=x, cost=unit_cost1[iVar]))
}

#ROI for iVar-th channel (hypothetical or historical)
roi_fn = function(iVar, type) {
  incr = incr_fn2(iVar=iVar, type=type)
  margin = lst_margin1[[gb]]
  spend = cost_fn2(iVar=iVar, type=type)
  roi = getROI(incr=incr, margin=margin, spend=spend)
  return(roi)
}

#create Hypothetical vs Historical bar chart for Incr Units, Cost and ROI
#type: Incr Units, Cost or ROI | scenario: Optimal or Planned/Simulated
hypo_vs_hist = function(type, scenario) {
  if (type == 'incr') {
    Hypothetical = unlist(lapply(1:length(lst_indep3), incr_fn2, type='hypo'))
    Historical = unlist(lapply(1:length(lst_indep3), incr_fn2, type='hist'))
    ylab = "Incremental Units"
    
  } else if (type == 'cost') {
    Hypothetical = unlist(lapply(1:length(lst_indep3), cost_fn2, type='hypo'))
    Historical = unlist(lapply(1:length(lst_indep3), cost_fn2, type='hist'))
    ylab = "Channel Cost"
    
  } else {
    Hypothetical = unlist(lapply(1:length(lst_indep3), roi_fn, type='hypo'))
    Historical = unlist(lapply(1:length(lst_indep3), roi_fn, type='hist'))
    ylab = "ROI"
  }
  
  #data for opt vs hist plot
  plotdata = data.frame(x=lst_indep3, 
                        Historical, 
                        Hypothetical, 
                        pct_chg=ifelse(Historical>0, round(100*(Hypothetical-Historical)/Historical, 1), 0)) %>%
  melt(id.vars='x', variable.name='name', value.name='y') %>% 
  mutate(type = ifelse(name=='pct_chg', 'text', 'bar'),
         textname = ifelse(name=='pct_chg', 'Historical', 'NULL'),
         colour = ifelse(name=='Historical','#FF5733', ifelse(name=='Hypothetical','#006400','NULL')),
         name = as.character(name))
  
  plotdata[plotdata$name=='Hypothetical', 'name'] = scenario #Hypothetical = Optimal or Planned

  #generate subtitle using total Y (not meaningful for ROI)
  if (!type == 'roi') {
    total_y_hist = numfmt(sum(plotdata[plotdata$name == 'Historical', 'y']))
    total_y_hypo = numfmt(sum(plotdata[plotdata$name == scenario, 'y']))
    subtitle = paste('Historical:', total_y_hist, '|', scenario, total_y_hypo)
  } else subtitle = NULL
  
  #export to JSON
  plot_to_json(plot_name = paste(gb, ' - ', ylab),
               category = paste(scenario, 'vs Historical'),
               plot_object = list(data = plotdata,
                                  xAxis = list(label='Channel'),
                                  yAxis = list(label=ylab,textformat='%'),
                                  viewMode = 'carousel',
                                  title = paste(gb, ylab, paste(scenario, 'vs Historical')),
                                  subtitle = subtitle,
                                  name = paste(paste(scenario, 'vs Historical'), gb, toupper(type)),
                                  chartType = 'bar'))
  
  return(plotdata)
}

#response curve builder
responseCurve = function(iVar, scenario) {
  #c1=initial value | c2=max value (x times average historical activity)
  c1 = 0
  c2 = 5
  
  if (est[iVar] == 0) return() #don't build response curve when model estimate is zero
  
  #generate arbitrary series of X and Y values (ORANGE line)
  xvec = seq(c1, c2*histx[iVar], length.out = 400)
  x = unlist(lapply(xvec, cost_fn1, unit_cost1[iVar]))
  y = unlist(lapply(xvec, incr_fn1, est[iVar]))
  df1 = data.frame(x, y, type='scatter', mode='lines', name='Incremental Units', colour='#FFA500')
  
  #historical point (RED dot)
  x = cost_fn1(histx[iVar], unit_cost1[iVar])
  y = incr_fn1(histx[iVar], est[iVar])
  df2 = data.frame(x, y, type='scatter', mode='markers', name='Historical', colour='#FF5733', row.names = NULL)
  
  #hypothetical point (GREEN dot)
  x = cost_fn1(hypox[iVar], unit_cost1[iVar])
  y = incr_fn1(hypox[iVar], est[iVar])
  df3 = data.frame(x, y, type='scatter', mode='markers', name=scenario, colour='#006400', row.names = NULL)
  
  #data for response curve
  plotdata = rbind(df1, df2, df3)
  var = lst_indep3[iVar]
  str1 = ifelse(scenario == 'Optimal', 'Increase', 'Increased')
  str2 = ifelse(scenario == 'Optimal', 'Reduce', 'Reduced')
  subtitle = paste( ifelse(df3$x > df2$x, str1, str2), 'allocation towards', var )
  
  #export to JSON
  plot_to_json(plot_name = paste(gb, '-', var),
               category = paste(scenario, 'Response Curve'),
               plot_object = list(data = plotdata,
                                  xAxis = list(label=var),
                                  yAxis = list(label='Incremental Units'),
                                  viewMode = 'carousel',
                                  title = paste(gb, 'Response Curve for', var),
                                  subtitle = subtitle,
                                  chartType = 'line'))
  
  return(plotdata)
}
