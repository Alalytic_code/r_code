##############################################################################
# Module: Linear Mixed-Effects Model (LME)                                   #
# Code: Source                                                               #
# Objective: Capture user inputs, run module scripts                         #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

#load generic packages
suppressWarnings(suppressMessages(library(dplyr)))
suppressWarnings(suppressMessages(library(data.table)))
suppressWarnings(suppressMessages(library(scales)))
suppressWarnings(suppressMessages(library(ggplot2)))
suppressWarnings(suppressMessages(library(reshape2)))
suppressWarnings(suppressMessages(library(plotly)))
suppressWarnings(suppressMessages(library(jsonlite)))



#script_path=dirname(parent.frame(2)$ofile)

#setwd(script_path)


options(warn = -1)
#output_stream=list()
#plot_stream = list()
#model_stream= list()




run_step <-function(outdir,step_name, ...){
  listVarname <- list(...)
  cat("\n", "running step \"", step_name,  "\" on analysis directory: ", outdir)
  #save messsage and errors to output directory
  msg = file(paste0(outdir, '/',step_name , '_message',  '.log'), open='wt')
  
  sink(msg, type=c('output', 'message'), append=F)
  
  # Assign ellipsis as local variable
  varnames=names(listVarname)
  assign('output_stream', list(), local_env)
  assign('outdir', outdir, local_env)
  
  for(varname in varnames){
    value = listVarname[[varname]]
    assign(varname,value, local_env)
  }
  
  
  #load generic functions
  source('funlib.R', verbose = FALSE, echo = FALSE)
  source('funmod.R', verbose = FALSE, echo = FALSE)
  
  
  #### Load the step source
  step_source=paste(step_name,'.r',sep = "")
  source(step_source)
  #output_stream(model_stream,plot_stream)
  
  # add logs to 
  info = list(type="console", name="console", path= paste0(step_name, '_message.log'))
  len = length(output_stream)
  output_stream[[len+1]] = info
  
  cat("\n", "produce result: ", toJSON(output_stream))
  cat("\n", "ran step \"", step_name,  "\" successfully on analysis directory: ", outdir)
  return(output_stream)
}
