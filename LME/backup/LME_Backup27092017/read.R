##############################################################################
# Module: Linear Mixed-Effects Model (LME)                                   #
# Code: read                                                                 #
# Objective: Read input data, print basic diagnostics                        #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

stime = as.POSIXct(Sys.time())

suppressWarnings(suppressMessages(library(data.table))) #fread

cat('\n', '==================================================')
cat('\n', '          ***** CONSOLE OUTPUT: READ ***** ')
cat('\n', '==================================================', '\n')

#read input file
rdata = fread(paste0(outdir, '/', datafile), data.table = FALSE)

#rename key columns
names_old = c(depvar, subject, period, geobrand)
names_new = c('depvarcol', 'subjectcol', 'periodcol', 'geobrandcol')
for (i in 1:length(names_old)) {
  names(rdata)[which(names(rdata) == names_old[i])] = names_new[i]
}

#create list of geobrand and indep vars
lst_geobrand = sort(unique(rdata$geobrandcol))
lst_indep1 = c(str_to_lst(lst_indep), mktsales)

#get start and end dates
pbounds1 = str_to_lst(pbounds)
pstart = as.Date(pbounds1[1], datefmt)
pend = as.Date(pbounds1[2], datefmt)

#print user selections
cat( '\n', paste0( 'input data file: ', datafile), '\n')
cat( '\n', paste0( nrow(rdata), ' records read from file'))
cat( '\n', paste0( 'period: ', period))
cat( '\n', paste0( 'start ', period, ': ', pstart))
cat( '\n', paste0( 'end ', period, ': ', pend), '\n')

#convert character variables to factors
lst_fac = lst_indep1[which(sapply(rdata[, lst_indep1], is.character))]
if (length(lst_fac) > 0) {
  rdata = mutate_each_(rdata, funs(as.numeric(as.factor(.))), lst_fac)
  cat( '\n', 'NOTE: below character variables were converted to factors', '\n', lst_fac)
}

lst_indep1 = setdiff(lst_indep1, c(lst_fac, mktsales))
rdata = rdata[names(rdata) %in% c(names_new, lst_indep1, lst_fac, mktsales)]

#apply period filter
rdata$periodcol = as.Date(rdata$periodcol, datefmt)
rdata = rdata[rdata$periodcol >= pstart & rdata$periodcol <= pend, ]
if (nrow(rdata) == 0) stop('Period variable not in selected format ', datefmt) else 
                      cat( '\n', paste( nrow(rdata), 'records between', pstart, 'and', pend), '\n')

#missing value imputation - replace NA with zero (numeric columns only)
if (as.integer(impute) == 1) rdata = mutate_if(rdata, is.numeric, funs(ifelse(is.na(.), 0, .)))

#outlier removal based on std dev of depvar
if (outlier > 0) {
  outlier1 = as.numeric(outlier)
  summ = group_by(rdata, geobrandcol) %>% summarise_each(funs(mean, sd), depvarcol) %>% mutate(ul = mean + outlier1*sd)
  rdata = merge(rdata, summ, by='geobrandcol')
  
  #print table of # of deleted records per geobrand
  foo = table(rdata[rdata$depvarcol > rdata$ul, ]$geobrandcol)
  if (nrow(foo) > 0) {
    cat('\n', paste('Outlier removal: No. of dropped records by', geobrand))
    print(foo)
  }
  rdata = rdata[rdata$depvarcol <= rdata$ul, ]
  rdata = rdata[!names(rdata) %in% c('mean', 'sd', 'ul')]
}

#total activity by geobrand-channel
activity = group_by(rdata, geobrandcol) %>% summarise_each_(funs(sum(.)), lst_indep1)
activity = melt(activity, id.vars = 'geobrandcol')
names(activity) = c('geobrand', 'channel', 'activity')
activity$geobrand = tolower(activity$geobrand)                   #to avoid mismatch of geobrand with cost file
activity$channel = tolower(activity$channel)                     #to avoid mismatch of channel names with cost file
activity = filter(activity, geobrand %in% tolower(lst_geobrand))

if (!is.null(costfile)) {
  #validate cost file
  costfile1 = paste0(outdir, '/', costfile)
  cols = header(costfile1)
  gbchk = tolower(uniq(costfile1, cols[1]))   #geobrands present in cost file
  varchk = tolower(uniq(costfile1, cols[2]))  #variables present in cost file
  if (!all(tolower(lst_geobrand) %in% gbchk)) {
    stop('Missing Brands/Geographies in cost file. Please ensure brand/geography names in cost file match with data file.')
  }
  if (!all(tolower(lst_indep1) %in% varchk)) {
    stop('Missing Variables in cost file. Please ensure variable names in cost file match with data file.')
  }

  #read cost file
  costdata = fread(costfile1, data.table = FALSE )
  names(costdata) = c('geobrand', 'channel', 'total_cost')
  costdata$geobrand = tolower(costdata$geobrand)
  costdata$channel = tolower(costdata$channel)
  costdata = filter(costdata, geobrand %in% tolower(lst_geobrand), channel %in% tolower(lst_indep1))
  
  #calculate unit cost = total cost/activity
  costdata = merge(activity, costdata, by = c('geobrand', 'channel'))
  costdata$unit_cost = round( ifelse(costdata$activity>0, costdata$total_cost/costdata$activity, 0), 4 )

} else {
  #if no cost file provided assume per unit cost of 1 for all channels
  costdata = expand.grid(tolower(lst_geobrand), tolower(lst_indep1), NA)
  names(costdata) = c('geobrand', 'channel', 'total_cost')
  costdata = merge(activity, costdata, by = c('geobrand', 'channel'))
  costdata$total_cost = costdata$activity
  costdata$unit_cost = 1
  cat('\n', 'NOTE: No cost information provided. ROI calculation will use same cost for all channels.', '\n')
}

#sort costdata to maintain same order as data file
sort1 = data.frame(geobrand = tolower(lst_geobrand), sort1 = seq(length(lst_geobrand)))
sort2 = data.frame(channel = tolower(lst_indep1), sort2 = seq(length(lst_indep1)))
costdata = merge( merge(costdata, sort2, by='channel'), sort1, by='geobrand' )
costdata = arrange(costdata, sort1, sort2)
costdata = costdata[!names(costdata) %in% c('sort1', 'sort2')]

#total cost required for ROI calculation | unit cost required for Optimization
total_cost = unit_cost = mlist(unique(costdata$geobrand))
for (gb in tolower(lst_geobrand)) {
  foo = costdata[costdata$geobrand == gb, ]
  total_cost[[gb]] = foo$total_cost
  names(total_cost[[gb]]) = foo$channel
  unit_cost[[gb]] = foo$unit_cost
  names(unit_cost[[gb]]) = foo$channel
}

#print spend summary by geobrand
for (gb in tolower(lst_geobrand)) {
  cat('\n', paste('----- Channel Spend Summary for Geo/Brand', gb, '-----'), '\n')
  print(costdata[costdata$geobrand == gb, ], row.names = FALSE)
}

#histogram for depvar and each indep var 
hist = function(var) {
  x = rdata[[var]] #avoid subsetting within aes stmt
  bin_width = 3.5 * sd(x) * length(x) ^ (-1/3) #dynamic binning based on # data-points and std dev
  k = ceiling((max(x) - min(x)) / bin_width)
  plotly_hist = 
    ggplot(rdata, aes(x = x, fill = geobrandcol)) + 
      geom_histogram(bins = k, color = 'Black', size = 0.2) +
        facet_wrap(~geobrandcol,scales = "free")
  
  #export to JSON
  title = paste('Histogram for', ifelse(var=='depvarcol', depvar, var))
  xAxis = list(label = ifelse(var=='depvarcol', depvar, var))
  yAxis = list(label = "# of Records")
  name=ifelse(var=='depvarcol', depvar, var)
  histogram = plot_to_json(name, 
               plotly_hist,
               category="Histogram",
               chart_type = 'bar', 
               xAxis = xAxis, 
               yAxis = yAxis, 
               title = title, 
               write_file = TRUE)
}

rplot1 = lapply(c('depvarcol', lst_indep1), hist)

#plot number of records by period
rplot2 = 
data.frame(table(rdata$geobrandcol, rdata$periodcol)) %>%
  mutate(geobrandcol=Var1, periodcol=Var2, No_of_records=Freq) %>%
  ggplot(aes(x=periodcol, y=No_of_records, fill=geobrandcol)) +
  geom_bar(stat = "identity", width = 0.8, colour = 'Black', size = 0.2) + 
  ggtitle(paste('No. of records by', period)) +
  labs(y = '# Records')+geom_text(aes(y=0, label=periodcol), angle=90, size=4, hjust=0) +
  geom_text(aes(y = 0, label = periodcol), angle=90, size=4, hjust=0)
  # title=paste('No. of records by', period)
  # xAxis=list(label="period",tickFormat="mm/dd/YYYY")
  # yAxis=list(label=var)
  # plot_to_json('number of records by period',category='frequency'
  #              ,chart_type='frequency',rplot2,xAxis=xAxis,yAxis=yAxis,title=title,write_file = TRUE )

#apply subject inclusion threshold ('static')
cat('\n', paste0('No. of observations by ', geobrand, ' (BEFORE applying subject inclusion threshold)'))
print(table(rdata$geobrandcol))

static1 = floor((as.numeric(static)/100) * length(unique(rdata$periodcol)))
n1 = length(unique(rdata$subjectcol))                                                 #before applying static
staticPlot = filter(data.frame(table(rdata$geobrandcol, rdata$subjectcol)), Freq > 0) #for charting
staticData = filter(staticPlot, Freq >= static1)                                      #for applying static filter
names(staticPlot) = names(staticData) = c('geobrandcol', 'subjectcol', 'n_by_sub')
rdata = merge(rdata, staticData, by=c('geobrandcol', 'subjectcol'))
n2 = length(unique(rdata$subjectcol))                                                 #after applying static
if (n1 > n2) cat('\n', paste( 'NOTE: dropped', n1-n2, subject, 'not meeting', static1, period, 'static'), '\n')

cat('\n', paste0('No. of observations by ', geobrand, ' (AFTER applying subject inclusion threshold)'))
print( table(rdata$geobrandcol) )
rdata = rdata[!names(rdata) == 'n_by_sub']

#plot showing no. of excluded subjects by geobrand (static-plot)
staticPlot$legend = ifelse(staticPlot$n_by_sub >= static1, 'Retained', 'Excluded')
rplot3 =
  ggplot(staticPlot, aes(x=n_by_sub, fill=legend)) + 
  geom_bar(colour = 'Black', size = 0.2)  + 
  facet_wrap(~geobrandcol, scales = "free") +
  labs(x = paste0('# ', period, 's of data'), y = paste0('Number of ', subject)) +
  # guides(fill = FALSE) + 
  ggtitle(paste('No. of excluded', subject, 'by', geobrand))

title = paste('No. of excluded', subject, 'by', geobrand)
xAxis = list(label=paste0('# ', period, 's of data'))
yAxis = list(label=paste0('Number of ', subject))

if (length(unique(staticPlot$legend)) > 1) {
  # excl_sub = plot_to_json_grp('Excluded Subjects',
  #                             rplot3,
  #                             category="Excludes",
  #                             chart_type='groupChart',
  #                             xAxis = xAxis,
  #                             yAxis = yAxis,
  #                             title = title,
  #                             write_file = TRUE)
} else {
  excl_sub = plot_to_json('Excluded Subjects',
                          rplot3,
                          category="Excludes",
                          chart_type='groupChart',
                          xAxis = xAxis,
                          yAxis = yAxis,
                          title = title,
                          write_file = TRUE)
}

#Exit Read
save(rdata, 
     lst_indep1,
     lst_fac,
     depvar, 
     subject, 
     period, 
     geobrand, 
     lst_geobrand, 
     datefmt, 
     mktsales,
     total_cost,
     unit_cost,
     file = paste0(outdir, '/read.rda'))

etime = as.POSIXct(Sys.time())
cat('\n', paste( 'Read took', round( difftime( etime, stime, units = 'mins'), 2), 'minutes' ), '\n')
