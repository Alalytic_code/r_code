##############################################################################
# Module: Linear Mixed-Effects Model (LME)                                   #
# Code: model2                                                               #
# Objective: Run MCMC model on best model data, produce final outputs        #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

stime = as.POSIXct(Sys.time())

suppressWarnings(suppressMessages(library(MCMCglmm)))      #MCMCglmm

source(file = 'funmod.R', verbose = FALSE, echo = FALSE)

######################################## Section 1: Definitions ###################################
zero_sd_vars = function(data) {
  x = apply(data[sapply(data, is.numeric)], 2 , function(a) sd(a, na.rm = T)==0)
  x = names(x[x==TRUE])
  return(x)
}
runMCMC = function(iCovStr) {
  
  #set core MCMC parameters
  min_V  = 0.01      #low variance: applied to channel vars for which user-specified priors exist
  max_V  = 1000      #high variance: applied to intercept, seas, trend and channels w/o user-specified priors
  nitt   = 1100      #number of mcmc iterations (default 13000)
  thin   = 1         #every x iteration will be used (default 10)
  burnin = 100       #discard first x iterations (default 3000)

  #create model formula based on covariance structure option
  #idh = uncorrelated random slopes | us = correlated random slopes
  form_fxd = as.formula(paste('depvarcol', paste0(c(lst_fxdvars, trend1, seas, compt1, lst_fac), collapse = ' + '), sep = ' ~ '))
  form_rnd = as.formula(ifelse(iCovStr==1, paste0(' ~ us(', paste(lst_rndvars, collapse=' + '), '):', 'subjectcol'),
                                           paste0(' ~ idh(', paste(lst_rndvars, collapse=' + '), '):', 'subjectcol')))
  
  #fixed effect priors on intercept, seas and trend have mean 0 
  #fixed effect priors on channel vars are user-specified (0 implies weak prior)
  #order of variables: intercept > indep vars > trend > seas > competitor > factors
  n_fxd = 1+length(lst_fxdvars)
  n_rnd = length(lst_rndvars)
  n_ctrl = length(seas) + length(trend1) + length(compt1) + length(lst_fac)
  mu = as.numeric( c(0, lst_prior2, rep(0, n_ctrl)) )             #means of fixed effect priors
  sigma = numeric(length(lst_prior2))                             #variances of fixed effect priors
  for (i in 1:length(lst_prior2)) {
    prior = lst_prior2[[i]]
    sigma[i] = ifelse(prior==0, max_V, min_V*prior)               #scale variances by prior means
  }

  #VB: covariance matrix for fixed effect priors | VG: G-matrix for random effects
  #weak priors imposed on random effects
  VB = diag(n_fxd + n_ctrl) * c(max_V, sigma, rep(max_V, n_ctrl))
  VG = diag(n_rnd)

  #run MCMCglmm
  set.seed(1)

  modobj = MCMCglmm(fixed = form_fxd, random = form_rnd, data = mdata,
                    prior = list(B = list(mu = mu, V = VB),
                                 R = list(V = 1, nu = min_V),
                                 G = list(G1 = list(V = VG, nu = min_V))),
                    nitt = nitt, thin = thin, burnin = burnin, pr = TRUE, verbose = FALSE
  ) 

  return(modobj)
}

#extract fixed effect estimates from an MCMC object
fxdMCMC = function(model) {
  fixed = data.frame(model$Sol)[, lst_fxdvars]
  fixed = apply(fixed, 2, mean) 
  fixed[fixed < 0] = 0 #set negative estimates to zero
  names(fixed) = paste0('fxd_', lst_indep2[[gb]])
  return(fixed)
}

#extract random effect estimates from an MCMC object
rndMCMC = function(model) {
  tmp = data.frame(model$Sol) 
  tmp = tmp[!names(tmp) %in% c(lst_fxdvars, seas, trend1)][, -1]
  tmp = apply(tmp, 2, mean)
  
  #column names formatted as "variable.subject.subject-value"
  rand = data.frame(var=sapply(strsplit(names(tmp), "[.]"), "[", 1), 
                    sub=sapply(strsplit(names(tmp), "[.]"), "[", 3), 
                    val=unname(tmp))
  
  #fill in NAs for missing random estimates
  comb = expand.grid(lst_rndvars, lst_geobrandsub[[gb]])
  names(comb) = c('var', 'sub')
  rand = merge(comb, rand, by=c('var', 'sub'), all.x = TRUE)
  
  #convert to wide format with var going across
  rand = reshape(rand, idvar = 'sub', timevar = 'var', direction = 'wide')
  rand[is.na(rand)] = 0
  names(rand) = c('subjectcol', paste0('rnd_', lst_rand2[[gb]]))
  
  #add fixed and random effect estimates
  fxd_rnd = fixed[paste0('fxd_', lst_rand2[[gb]])]
  rand[, -1] = sweep(rand[, -1], 2, fxd_rnd, FUN='+')
  rand = mutate_if(rand, is.numeric, funs(ifelse(. < 0, 0, .))) #set negative estimates to zero
  return(rand)
}

#converts a two-level list to data frame
lst_to_df = function(list) {
  df = data.frame(geobrand=character(0), variable=character(0), value=numeric(0))
  for (i in 1:length(list)) df = rbind(df, data.frame(geobrand = names(list[i]), 
                                                      variable = names(list[[i]]), 
                                                      value = list[[i]]))
  rownames(df) = NULL
  return(df)
}

########################################### Section 2: MCMC Run ###############################################
load(file = paste0(outdir, '/model1.rda'))


#list of model vars to be dropped for each geobrand
drop_vars1 = rlist(drop_vars, lst_geobrand, lst_indep1)

#cat("..........")

#set model vars excl drop vars
lst_indep2 = mlist(lst_geobrand)
for (gb in lst_geobrand) lst_indep2[[gb]] = lst_indep1[drop_vars1[[gb]] == 0]

#set randomized variables (remove dropped fixed effect terms)
lst_rand2 = mlist(lst_geobrand)
for (gb in lst_geobrand) lst_rand2[[gb]] = intersect(lst_rand1, lst_indep2[[gb]])

#set fixed effect priors for each geobrand
lst_prior1 = rlist(lst_prior, lst_geobrand, lst_indep1)

#cat('\n', 'List of RETAINED variables', '\n'); print(lst_indep2)
#cat('List of priors', '\n'); print(lst_prior1)

#initialize list objects to store MCMC results
contribMCMC = roiMCMC = incrPrd = incrSub = fitPrd = fxdOpt = mlist(lst_geobrand)

#run MCMC for each geobrand using best model params
for (gb in lst_geobrand) {

  iParam = params[params$geobrand==gb & params$Model==best_model[[gb]], ]
  setParams(iParam$geobrand, iParam$Lambda, iParam$TrendSeas, lst_indep2[[gb]], lst_rand2[[gb]])

  lst_prior2 = lst_prior1[[gb]]                            #filter for geobrand
  lst_prior2 = lst_prior2[lst_indep2[[gb]]]                #exclude dropped vars

  total_cost1 = total_cost[[gb]]
  total_cost1 = total_cost1[lst_indep2[[gb]]]
  
  #run MCMCglmm
  modMCMC = runMCMC(iParam$CovStr)

  cat('\n', '-----------------------------------------------------')
  cat('\n', paste('***** MCMC Model Summary for', gb, ' *****'))
  cat('\n', '-----------------------------------------------------', '\n')
  print(summary(modMCMC))
  
  #prepare decomp data
  pred = predict(modMCMC, marginal = NULL) #MCMC pred does not add up to actual!
  fixed = fxdMCMC(modMCMC)
  keepvars = c('depvarcol', 'pred', 'periodcol', lst_fxdvars, names(fixed), mean_y, lst_mean_fxd)
  data1 = cbind(mdata, t(fixed), pred=untransform(pred))[, keepvars]
  resDecomp = decomp(df=data1, xcols=lst_fxdvars, ecols=names(fixed), mcols=lst_mean_fxd) #decomps at observation level

  #channel % contributions
  incr = colSums(resDecomp[, lst_fxdvars])
  incr_overall = round(100 * (incr / sum(resDecomp$pred)), 2)
  incr_overall = c(100 - sum(incr_overall), incr_overall) #base = 100 - total incr
  names(incr_overall) = c('base', lst_indep2[[gb]])
  contribMCMC[[gb]] = incr_overall
  
  #channel ROI
  roiMCMC[[gb]] = round((lst_margin1[[gb]] * incr) / total_cost1, 2)
  names(roiMCMC[[gb]]) = lst_indep2[[gb]]
  
  #model estimates required for optimization
  fxdOpt[[gb]] = fixed
  names(fxdOpt[[gb]]) = lst_indep2[[gb]]

  #print overall results
  cat( '\n', '----- Channel Contributions -----', '\n' ) 
  print(contribMCMC[[gb]])
  cat( '\n', paste0('Total Incremental Units = ', round(sum(contribMCMC[[gb]][-1]), 2), '%' ), '\n' )
  cat( '\n', '----- Channel ROI -----', '\n' ) 
  print(roiMCMC[[gb]])

  #decomps by period - required for area chart
  incr_by_prd = group_by(resDecomp, periodcol) %>% summarise_each_(funs(sum(.)), c('pred', lst_fxdvars))
  incr_by_prd$base = incr_by_prd$pred - rowSums(incr_by_prd[, lst_fxdvars])  #base = pred minus sum of channels
  names(incr_by_prd) = c('periodcol', 'pred', lst_indep2[[gb]], 'base')      #remove decay rate from col names
  incrPrd[[gb]] = incr_by_prd
  
  #actual & pred Y by period - required for fit chart
  pred_by_prd = mutate(resDecomp, depvarcol=untransform(depvarcol)) %>% 
    group_by(periodcol) %>% summarise(pred=sum(pred), actual=sum(depvarcol))
  fitPrd[[gb]] = pred_by_prd
  
  #decomps by subject
  rand = rndMCMC(modMCMC)
  keepvars = c('depvarcol', lst_rndvars, names(rand), mean_y, lst_mean_rnd)
  data1 = merge(mdata, rand, by = 'subjectcol')[, keepvars]
  resDecomp = decomp(df=data1, xcols=lst_rndvars, ecols=names(rand)[-1], mcols=lst_mean_rnd)
  
  #adjustment to make subject level incr add up to overall incr
  tmp = group_by(resDecomp, subjectcol) %>% summarise_each_(funs(sum(.)), lst_rndvars)
  incr_by_sub = colSums(tmp[, -1])
  rnd_incr = incr[lst_rndvars]
  factor = ifelse(is.nan(rnd_incr/incr_by_sub), 0, rnd_incr/incr_by_sub)
  tmp[, -1] = sweep(tmp[, -1], 2, factor, FUN='*')
  incrSub[[gb]] = tmp

}

######################################## Section 3: Final Outputs & Plots ###########################################
#write final outputs to CSV
contribMCMC = lst_to_df(contribMCMC)
write_csv("model_overall", contribMCMC, row.names = FALSE, write_file=TRUE)
roiMCMC = lst_to_df(roiMCMC)
write_csv("model_ROI", roiMCMC, row.names = FALSE, write_file=TRUE)
fxdOpt = lst_to_df(fxdOpt)
write_csv("model_estimates", fxdOpt, row.names = FALSE, write_file=TRUE)

#convert area and fit chart data to long format
areaData = fitData = data.frame()
for (gb in lst_geobrand) {
  tmp = melt(incrPrd[[gb]], id.vars = 'periodcol', variable.name = 'channel') 
  tmp$geobrand = gb
  areaData = rbind(areaData, tmp)
  
  tmp = melt(fitPrd[[gb]], id.vars = 'periodcol', variable.name = 'depvarcol') 
  tmp$geobrand = gb
  fitData = rbind(fitData, tmp)
}

#for subject level, a separate CSV file for each geobrand
for (gb in lst_geobrand) {
  tmp = incrSub[[gb]]
  names(tmp) = c('subjectcol', lst_rand2[[gb]])
  tmp$total = rowSums(tmp[, -1])
  write_csv(paste('model_subject_', gb), tmp, row.names = FALSE, write_file=TRUE)
}

#ROI bar chart
m2plot1 = 
  arrange(roiMCMC, geobrand, -value) %>%
  mutate(variable1 = factor(paste(variable, geobrand, sep='.'), levels=unique(paste(variable, geobrand, sep='.')))) %>%
  ggplot(aes(x = variable1, y = value, fill = geobrand)) + 
  facet_wrap(~ geobrand, scales = "free", ncol=2) +
  geom_bar(stat = 'identity', colour = 'Black', size = 0.2) + 
  ggtitle('Channel ROI by Geo/Brand') + 
  theme(plot.title = element_text(hjust = 0.5, size = 18, face = 'bold'), 
        axis.text.x=element_blank(),axis.ticks.x=element_blank(),
        axis.text.y = element_text(size=8),
        legend.position = "none",
        strip.text.x = element_text(size = 18),
        panel.border = element_rect(colour = "black", fill=NA, size=1)) +
  geom_text(aes(y=0, label=variable), angle=90, size=5, hjust=0) + 
  labs(x = 'Channel', y = 'ROI')
  title=paste('Channel ROI by Geo/Brand')
  xAxis=list(label='VARIABLES')
  yAxis=list(label='')
  plot_to_json('ROI_bar_chart',
                 m2plot1,
                 category="ROI",
                 chart_type='groupChart',
                 chart_type_new='groupChart',
                 xAxis=xAxis,yAxis=yAxis,
                 title=title,
                 write_file = TRUE)

#contribs waterfall
m2plot2 = 
  arrange(contribMCMC, geobrand, -value) %>% group_by(geobrand) %>% 
  mutate(id=seq_along(value), 
         cume=cumsum(value), 
         lcume=lag(cume), 
         variable=factor(variable, levels=unique(variable))) %>%
  mutate_each(funs(ifelse(is.na(.), 0, .)), lcume) %>%
  ggplot() + 
  facet_wrap(~ geobrand, scales="free", ncol=2) +
  geom_rect(aes(x=variable, xmin=id-0.45, xmax=id+0.45, ymin=cume, ymax=lcume, fill=geobrand),colour='Black',size=.2) + 
  ggtitle('Channel Contributions Waterfall Chart') + 
  theme(plot.title = element_text(hjust=0.5, size=18, face='bold'), 
        axis.text.x = element_text(size=12, colour="black"),
        axis.text.y = element_text(size=12, colour="black"),
        strip.text.x = element_text(size=18),
        legend.position = "none", panel.border = element_rect(colour="black", fill=NA, size=1)) +
        labs(x = 'Channel', y = '% Contribution')

#area chart - incremental units by period
m2plot3 = 
  arrange(areaData, geobrand, periodcol, value) %>%
  mutate(channel=factor(channel, levels=sort(unique(channel))), periodcol=as.POSIXct(periodcol)) %>%
  ggplot(aes(x = periodcol, y = value, fill = channel)) + 
  facet_wrap(~geobrand, scales = "free", ncol = 2) +
  geom_area(colour = "black", size = 0.2, stat = "identity", position = position_stack(reverse=T)) + 
  ggtitle(paste('Channel Contributions by', period)) +
  scale_x_datetime(date_labels = "%Y-%m-%d",
                   breaks = date_breaks("30 day")) +
  theme(axis.text.x = element_text(size=10,angle=30, colour="black", vjust=1, hjust=1),
        plot.title = element_text(hjust=0.5, size=18, face='bold'), 
        axis.text.y = element_text(size=10),
        strip.text.x = element_text(size=18),
        panel.border = element_rect(colour = "black", fill=NA, size=1)) + 
  labs(x = 'Channel', y = 'Incremental Units')
# plot to json 
  title=paste('Channel Contributions by', period)
  xAxis=list(label=period,tickFormat="%b-%d-%Y", type="date")
  yAxis=list(label='Incremental Units')
  plot_to_json_area('area_chart', m2plot3, chart_type='area_chart', category="Channel Contributions", geobrand_num=length(sort(unique(areaData$geobrand))),channel_num=length(sort(unique(areaData$channel))),xAxis=xAxis,yAxis=yAxis,title=title, write_file=TRUE )
  
#fit chart - actual vs predicted depvar by period
fitData$R2 = NA
for (gb in lst_geobrand) fitData[fitData$geobrand==gb, ]$R2 = 
  RSquared(
  Target = fitData[fitData$geobrand==gb & fitData$depvarcol=='actual', 'value'],
  Predicted = fitData[fitData$geobrand==gb & fitData$depvarcol=='pred', 'value']
  )
fitData$label = paste0(fitData$geobrand, ' (R-squared=', round(fitData$R2, 1), ')') #add R2 to panel labels

m2plot4 = 
  mutate(fitData, periodcol=as.POSIXct(periodcol)) %>%
  ggplot(aes(x = periodcol, y = value, colour = depvarcol)) +
  facet_wrap(~label, scales = "free", ncol = 2) +
  geom_line(stat = 'identity', size = 0.8, linetype = 1) + 
  ggtitle(paste('Fit Chart: Actual vs Predicted', depvar)) +
  theme(plot.title = element_text(hjust = 0.5, size = 18, face = 'bold'),
        axis.text.x = element_text(size=10, colour="black", angle=90),
        axis.text.y = element_text(size=10, colour="black"),
        strip.text.x = element_text(size=14), legend.position="none",
        panel.border = element_rect(colour="black", fill=NA, size=1)) +
  labs(x = period, y = depvar) + 
  scale_color_discrete(name = "Legend") +
  scale_x_datetime(date_labels = "%Y-%m-%d",
                   breaks = date_breaks("30 days"))
  #scale_y_continuous(limits = c(0, NA))
title=paste('Fit Chart: Actual vs Predicted', depvar)
xAxis=list(label=period,tickFormat="%b-%d-%Y", type="date")
yAxis=list(label='')
plot_to_json_fit('fit_chart',chart_type='fit_chart', category="Fit", m2plot4,xAxis=xAxis,yAxis=yAxis,title=title, write_file=TRUE )

#export plots to HTML (comment in test mode)
#plot_to_html(m2plot1, 'ROI_bar_chart')
#plot_to_html(m2plot2, 'waterfall_chart')
#plot_to_html(m2plot3, 'area_chart')
#plot_to_html(m2plot4, 'fit_chart')

## plot to json

#plot_to_json('ROI_bar_chart',chart_type='groupChart', m2plot1, category="ROI",  write_file=TRUE )

plot_to_json('waterfall_chart',chart_type='bar', m2plot2, category="Waterfall",  write_file=TRUE)



#Exit Model2
save(pdata, 
     fxdOpt, 
     lst_cost, 
     geobrand,
     depvar,
     subject,
     period,
     lst_indep1,
     lst_indep2,
     lst_geobrand, 
     lst_geobrandsub,
     file = paste0(outdir, '/model2.rda'))
etime = as.POSIXct(Sys.time())
cat( '\n', paste( 'Model took', round( difftime( etime, stime, units = "mins"), 2), 'minutes' ), '\n' )
