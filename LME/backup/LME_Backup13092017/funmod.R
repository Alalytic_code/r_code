##############################################################################
# Objective: Define common functions used by LMER and MCMC                   #
##############################################################################

#setup model environment based on various options
#iGeoBrand: geo/brand being modeled | iLambda: decay rate applied | iTrendSeas: options 1 through 4
#fxdvars: list of fixed effect vars | rndvars: list of randomized vars
setParams = function(iGeoBrand, iLambda, iTrendSeas, fxdvars, rndvars) {
  mdata <<- pdata[pdata$geobrandcol == iGeoBrand, ]  #subset input data for geobrand
  
  remove_vars = zero_sd_vars(mdata)
  mdata <<- mdata[!names(mdata) %in% remove_vars]
  
  #create list of model vars based on decay rate
  dk <<- ifelse(iLambda == 0, '', as.character(iLambda*10))
  lst_fxdvars <<- setdiff(paste0(fxdvars, dk), remove_vars)
  lst_rndvars <<- setdiff(paste0(rndvars, dk), remove_vars)
  
  #include trend, seasonality and competitor where applicable
  if (iTrendSeas %in% c(1,2)) {
    if (length(lst_seas[[iGeoBrand]]) == 0) seas <<- NULL else seas <<- lst_seas[[iGeoBrand]]
  } else {
    if (is.null(mktsales1)) seas <<- NULL else seas <<- 'seasfact'
  }
  
  if (iTrendSeas %in% c(1,3)) trend1 <<- 'trend' else trend1 <<- NULL
  if (is.null(mktsales1)) compt1 <<- NULL else compt1 <<- 'compt'
  
  #means for unmeancentering
  if (as.integer(use_mc) == 1) {
    mean_y <<- 'depvarcol_mean'
    lst_mean_fxd <<- lst_mean[lst_mean %in% paste0(lst_fxdvars, '_mean')]
    lst_mean_rnd <<- lst_mean[lst_mean %in% paste0(lst_rndvars, '_mean')]
  } else {
    mean_y <<- NULL
    lst_mean_fxd <<- NULL
    lst_mean_rnd <<- NULL
  }
}

#return Y in real space
untransform = function(y) {
  if (as.integer(use_mc) == 1) y = y + mdata[[mean_y]]
  if (as.integer(use_log) == 2) y = exp(y) - 1
  return(y)
}

#calculate decomps from model data and model estimates
#df=input data frame | xcols=data columns | ecols=estimate columns | mcols=mean of X's
decomp = function(df, xcols, ecols, mcols) {
  use_log = as.integer(use_log)
  use_mc = as.integer(use_mc)
  
  if (use_mc==0) {
    
    if (use_log %in% c(0,1)) {
      #use_mc=0 and use_log=0 or 1
      df[, xcols] = df[, xcols] * df[, ecols]
    } 
    else if (use_log==2) {
      #use_mc=0 and use_log=2
      model_log_y = pred
      actual_log_y = mdata$depvarcol          
      vdx = df[, xcols] * df[, ecols]
      
      vdr = -1 * sweep(vdx, 1, model_log_y, FUN = '-')            #subtract each vdx from model_log_y
      factor = exp(actual_log_y) / exp(model_log_y)               #adjustment factor to align actual and pred
      pred_y = factor * (exp(model_log_y) - 1)                    #reversal of log(1+y)
      pred_vdr = factor * (exp(vdr) - 1)                          #reversal of log(1+x)
      df[, xcols] = -1 * sweep(pred_vdr, 1, pred_y, FUN = '-')    #subtract each VDR(x) from pred(Y)
    }  
    
  } else if (use_mc==1) {
    
    if (use_log %in% c(0,1)) {
      #use_mc=1 and use_log=0 or 1
      df[, xcols] = (df[, xcols] + df[, mcols]) * df[, ecols]
    } 
    else if (use_log==2) {
      #use_mc=1 and use_log=2
      model_log_y = pred + df[, mean_y]                    
      actual_log_y = mdata$depvarcol + df[, mean_y]        
      vdx = (df[, xcols] + df[, mcols]) * df[, ecols]     
      
      vdr = -1 * sweep(vdx, 1, model_log_y, FUN = '-')            #subtract each vdx from model_log_y
      factor = exp(actual_log_y) / exp(model_log_y)               #adjustment factor to align actual and pred
      pred_y = factor * (exp(model_log_y) - 1)                    #reversal of log(1+y)
      pred_vdr = factor * (exp(vdr) - 1)                          #reversal of log(1+x)
      df[, xcols] = -1 * sweep(pred_vdr, 1, pred_y, FUN = '-')    #subtract each VDR(x) from pred(Y)
    }
    
  }
  return(df)
}

#compute 'generic' R-squared from actual and predicted Y
RSquared = function(Target, Predicted) {
  res = Target - Predicted
  RSS = t (res) %*% res #sum of squares using scalar product
  mod = Predicted - mean (Predicted)
  MSS = t (mod) %*% (mod)
  RSQ = 1.0 - (RSS[1,1]/(RSS[1,1]+MSS[1,1]))
  return (RSQ*100)
}
