##############################################################################
# Module: Linear Mixed-Effects Model (LME)                                   #
# Code: prep                                                                 #
# Objective: Prepare modeling dataset, applying transformations              #
# Author: Ambar Nag                                                          #
# Last Updated:                                                              #
##############################################################################

stime = as.POSIXct(Sys.time())

########################################### Start Definitions ########################################
#Calculate *monthly* seasonality factor from market sales
#x=data frame containing geobrand, mth_num and mktsales (in that order)
seasfact = function(x) {
  m = group_by(x, geobrandcol, mth_num) %>% summarise_each_(funs(m=mean(.)), mktsales1) #avg mktsales by month (1-12)
  y = summarise_each_(x, funs(y=mean(.)), mktsales1)                                    #global avg mktsales
  seas = merge(m, y)
  seas$seasfact = seas$m/seas$y                                                         #seasfact = month avg/global avg
  seas = seas[!names(seas) %in% c('m', 'y')]
  return(seas)
}

#Apply Adstock Decay
#x=data frame containing id and var | lambda=decay rate
decay_lt = function(x, lambda) {
  dk = numeric(nrow(x))
  dk[1] = x[1, 2]
  first.id = c(TRUE, !x[-nrow(x), 1] == x[-1, 1]) #boolean vector for by-group processing
  
  for (i in 2:nrow(x)) {
    if (first.id[i]) dk[i] = x[i, 2] else dk[i] = lambda*x[i, 2] + (1-lambda)*dk[i-1]
  }
  return(dk)
}

#standardize a series into [0,1] range
stand = function(x) {
  range_x = max(x) - min(x)
  if (range_x > 0) res = (x - min(x))/(range_x) else res = x
  return(res)
}

#plot model variables time trend
SeasPlotFun = function(var) {
  plot_obj=ungroup(seasplot) %>% mutate(value=seasplot[[var]], period1=as.POSIXct(periodcol)) %>% 
  ggplot(aes(x = period1, y = value, colour = geobrandcol)) +
  facet_wrap(~geobrandcol, scales = "free", ncol = 2) +
  geom_line(stat = 'identity', size = 0.8, linetype = 1) +
  ggtitle(paste('Seasonality Plot for', var)) +
  theme(plot.title = element_text(hjust = 0.5, size = 18, face = 'bold'),
        axis.text.x = element_text(size=8, colour="black", angle=90),
        axis.text.y = element_text(size=8, colour="black"),
        strip.text.x = element_text(size=18), legend.position="none",
        panel.border = element_rect(colour="black", fill=NA, size=1)) +
  labs(x = period, y = var) + scale_color_discrete(name = "Legend") #+
  #scale_x_datetime(date_labels = "%Y-%m-%d",clbreaks = date_breaks("30 days"))
  name=ifelse(var=='depvarcol', depvar, var)
  title=paste('Seasonality Plot for <span>', toupper(name),'</span>')
  xAxis=list(label=period,tickFormat="%b-%d-%Y", type="date")
  yAxis=list(label=name)
  plot_to_json(paste0('Seasonality_',name),
               chart_type = 'panel_chart',
               plot_obj,
               xAxis = xAxis,
               yAxis = yAxis,
               title = title,
               write_file = TRUE )
  
}
########################################### End Definitions ########################################

load(file = paste0(outdir, '/read.rda'))
pdata = rdata

cat("\n", '==================================================')
cat("\n", '          ***** CONSOLE OUTPUT: PREP ***** ')
cat("\n", '==================================================', "\n")

#run seasonality plot
seasplot = group_by(pdata, geobrandcol, periodcol) %>% summarise_each_(funs(sum(.)), c('depvarcol', lst_indep1))
pplot1 = lapply(c('depvarcol', lst_indep1), SeasPlotFun)

#create trend variable
pdata$trend = as.numeric(as.factor(pdata$periodcol))

#seas as period dummies
pdata$prd_num = format(as.Date(pdata$periodcol), "%Y%m%d")
lst_prd_num = sort(unique(pdata$prd_num))
lst_dummy = paste0('D', lst_prd_num)
for (l in lst_prd_num) pdata[, paste0('D', l)] = ifelse(pdata$prd_num == l, 1, 0)

#list of monthly dummies to be used based on seas_cutoff
seasplot$group = c(TRUE, !seasplot[-nrow(seasplot), 'geobrandcol'] == seasplot[-1, 'geobrandcol'])
seasplot$abs_pct_chg = ifelse(seasplot$group, 0, 100 * abs((seasplot$depvarcol/lag(seasplot$depvarcol) - 1)))
lst_seas = mlist(lst_geobrand)
#if abs change from previous period is higher than seas_cutoff, include dummy in lst_seas
seas_cutoff1 = as.numeric(seas_cutoff) 
cat('\n', paste0('Seasonality cutoff = ', seas_cutoff1, '%'), '\n')
for (gb in lst_geobrand) {
  tmp = seasplot[seasplot$geobrandcol == gb, ]
  lst_seas[[gb]] = lst_dummy[which(tmp$abs_pct_chg > seas_cutoff1)]
  cat('\n', paste('Seasonality dummies for Brand/Geo', gb), '\n')
  if (length(lst_seas[[gb]]) > 0) cat(lst_seas[[gb]], '\n') else cat('None', '\n')
}

#add competitor sales and seasonality factor (if market sales provided by user)
if (!is.null(mktsales1)) {
  pdata$compt = pdata[[mktsales1]] - pdata$depvarcol #compt = market - target brand 
  pdata$mth_num = format(as.Date(pdata$periodcol), '%m')
  seas = seasfact(select_(pdata, 'geobrandcol', 'mth_num', mktsales1))
  pdata = merge(pdata, seas, by=c('geobrandcol', 'mth_num'))
}

#create a column combining geobrand and subject (required for decay and mean centering)
pdata$geobrandsub = paste(pdata$geobrandcol, pdata$subjectcol, sep = '.')

############################################# Adstock Decay ############################################
#applied to factors only
usedecaylabel = ifelse(as.integer(use_decay) == 1, TRUE, FALSE)
cat('\n', paste( 'decay is set to', usedecaylabel), '\n')

if (as.integer(use_decay) == 1)
{
  dkbounds1 = str_to_lst(decay_bounds)
  min_decay = as.numeric(dkbounds1[1])/100
  max_decay = as.numeric(dkbounds1[2])/100
  lst_lambda = seq(min_decay, max_decay, 0.1)
  cat('\n', paste( 'decay rates from', min_decay, 'to', max_decay, 'will be modeled'), '\n')
  
  #expand to all subject-all period and sort by subject-period
  comb = expand.grid(unique(pdata$geobrandsub), unique(pdata$periodcol))
  names(comb) = c('geobrandsub', 'periodcol')
  data1 = merge(comb, pdata, by=c('geobrandsub', 'periodcol'), all.x = TRUE)
  data1[is.na(data1)] = 0
  data1 = arrange(data1, geobrandsub, periodcol)

  #initialize a temp data frame 
  lst_var = as.vector(t(outer(lst_indep1, lst_lambda*10, paste0)))
  tempdf = data.frame( matrix( NA, ncol = length(lst_var), nrow = nrow(data1) ) )
  names(tempdf) = lst_var
  
  #v=variable, l=lambda
  i = 1
  for (v in lst_indep1) {
    for (l in lst_lambda) {
      tempdf[i] = decay_lt(data1[, c('geobrandsub', v)], l)
      i = i + 1
    }
  }
  
  data1 = cbind(data1, tempdf)
  tmp = pdata[, c('geobrandsub', 'periodcol')]
  pdata = merge(data1, tmp, by=c('geobrandsub', 'periodcol')) #drop extra records

} else {
  lst_lambda = 0
  lst_var = lst_indep1
}

############################################# Log Transformation ############################################
useloglabel = ifelse(use_log==0, 'None', ifelse(use_log==1, 'Semi-log: Y = a + b * log(X)', 
                                                            'Log-linear: log(Y) = a + b * log(X)'))
cat('\n', paste( 'log transformation is set to', useloglabel), '\n')

if (as.integer(use_log) > 0) {
  if (as.integer(use_log) == 1) {
    pdata = mutate_each_(pdata, funs(logtr(.)), lst_var) 
  } else {
    pdata = mutate_each_(pdata, funs(logtr(.)), c('depvarcol', lst_var))
  }
}

################################################# Mean Centering ############################################
#applied to depvar & indep vars
use_mc1 = as.integer(use_mc)
if (use_mc1 == 0) usemclabel = 'No standardization done' else
if (use_mc1 == 1) usemclabel = 'Standardization using mean-centering: x - mean(x)' else 
                  usemclabel = 'Standardization using min-max: x - min(x)/max(x) - min(x)'
cat('\n', usemclabel, '\n')

if (use_mc1 == 1) {
  means = group_by(pdata, geobrandsub) %>% summarise_each_(funs(mean = mean(.)), c('depvarcol', lst_var)) 
  lst_mean = names(means[, -1])
  pdata = merge(pdata, means, by='geobrandsub')
  pdata[, c('depvarcol', lst_var)] = pdata[, c('depvarcol', lst_var)] - pdata[, lst_mean]
} else if (use_mc1 == 2) {
  pdata = group_by(pdata, geobrandsub) %>% mutate_each_(funs(stand), c('depvarcol', lst_var))
}

pdata = pdata[!names(pdata) %in% c('prd_num', 'mth_num','geobrandsub')] #get rid of temp columns

#Exit Prep
save(pdata, 
     geobrand, 
     depvar, 
     lst_indep1,
     lst_fac,
     subject, 
     period, 
     datefmt, 
     use_log, 
     use_mc,
     lst_geobrand,
     lst_seas, 
     lst_lambda,
     mktsales1,
     total_cost,
     lst_cost,
     file = paste0(outdir, '/prep.rda'))
if (exists('lst_mean')) append(lst_mean, paste0(outdir, '/prep.rda')) 

etime = as.POSIXct(Sys.time())
cat( '\n', paste( 'Prep took', round( difftime( etime, stime, units = "mins"), 2), 'minutes' ), '\n' )
