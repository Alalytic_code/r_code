

#' @serializer unboxedJSON
#* @post /run
run<-function(rmodels_path, process_id, module_name, res, req,...){


    if(!exists('source_real')){
        assign('source_real', source, .GlobalEnv)
    }
  
  
  
  
   local_env <- .GlobalEnv #attach(new.env(), name=process_id)
   
   local_env$local_env = local_env

    l = list(...)
    
    cat("\n", "Running Step")
    cat("\n", "------------")
    cat("\n", "Process id: ", process_id)
    cat("\n", "Step name: ", l[['step_name']] )
    cat("\n", "Module: ", module_name)
    cat("\n", "User Dir: ", l[['outdir']],"\n")
    
    varnames = names(l)
    for( varname in varnames){
        value = l[[varname]]
        value_len = length(value)
        value_type = typeof(value)
        if(value_len > 1 && (value_type %in% c("integer", "character"))){
            value_list = list()
            for (count in 1:value_len){
                value_list[[count]] = value[[count]]
            }
            value = value_list
            l[[varname]] = value
        }
    }
    
    assign('source', function(filepath, ...){
       source_real(paste(rmodels_path, module_name, filepath , sep='/'), local=local_env, ...)
    }, envir=local_env)

    success = FALSE
   
    
    result = tryCatch({
      
        success = TRUE
        source( 'src.r')
        result = do.call(run_step, l)
        
        # clean up the environment
        sink()
        rm(list=ls(), envir = local_env)
        cat("\n", "Completed! Process:", process_id, ' Step name:', l[['step_name']])
        
        return(result)
    },error = function(e) {
        print('Error',geterrmessage())
        result = list(list(type="console", name="console", content=geterrmessage()))
        res$status <- 500
        success = TRUE
        
        # clean up the environment
        sink()
        rm(list=ls(), envir = local_env)
        
        cat("\n", "Failed! Process:", process_id, ' Step name:', l[['step_name']], "\n")
        return(result)
    })
}
