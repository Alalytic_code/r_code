
pdata <- copy(mdata)
pdata[[depvar]] <- ifelse(pdata[[depvar]]==event,1,0)

if(Binning == 'Y') {
  #Creates bins of continuous variables
  varsToBin <- lst_number[lst_number != depvar]
  bins <- list()

  for (k in 1:length(varsToBin)) {
    bins[[k]] <- create.bins.only(x=varsToBin[k],mydata=pdata)
    if(length(bins[[k]]) != 1) {
      pdata <- apply.bins.only(x=varsToBin[k],bin=bins[[k]][["ivtable"]],mydata=pdata)
    }
    
  }
  
  names(bins) <- varsToBin
  
  #Drop original variables binned if they contain missing values
  pdata[ ,c(varsToBin):=lapply(.SD,function(x) if(is.na(sum(x))) NULL else x),.SDcols=varsToBin]
  
  #Create bins in scoring data
  if(opt_score == 'Y') {
    
    for (k in 1:length(varsToBin)) {
      if(length(bins[[k]]) != 1) {
        sdata <- apply.bins.only(x=varsToBin[k],bin=bins[[k]][["ivtable"]],mydata=sdata)
      }
      
    }
    
    #Drop original variables binned if they contain missing values
    sdata[ ,c(varsToBin):=lapply(.SD,function(x) if(is.na(sum(x))) NULL else x),.SDcols=varsToBin]
    
  }
  
}


#Create dummy variables for categorical variables and keep in feature data
if (opt_score == "Y") {
  
  pdata$score <- 0
  sdata$score <- 1
  alldata <- rbind(pdata,sdata)
  myform <- paste0(depvar,"~",".")
  dmy <- dummyVars(myform, data=alldata)
  features <- data.frame(predict(dmy,newdata=pdata))
  sdata <- data.frame(predict(dmy,newdata=sdata))
  features$score <- NULL
  sdata$score <- NULL
  
} else {
  
  myform <- paste0(depvar,"~",".")
  dmy <- dummyVars(myform, data=pdata)
  features <- data.frame(predict(dmy,newdata=pdata))
  
}


#Create response vector and recode it to Event and Non.Event. Only on modeling data.
resp <- pdata[ ,get(depvar)]
resp <- factor(ifelse(resp==1,"Event","Non.Event"),levels = c("Event","Non.Event"))
Event <- "Event"
nonEvent <- "Non.Event"

cat('\n', 'NOTE: Dummy variables created for Character variables ', '\n')

#Remove Variables with nearly zero variance in modeling data.
nzv <- nearZeroVar(features)
if(length(nzv) != 0) {
  cat('\n', '1. Following Variables removed due to nearly zero variance', '\n')
  cat(' ',colnames(features)[nzv],'\n')
  features <- features[ ,-nzv]
} else cat('\n', "1. None of the variables removed due to nearly zero variance", '\n') 

#Remove Variables with multicolinearity in modeling data.
getcor <- cor(features)
highlyCorDescr <- findCorrelation(getcor, cutoff = .75)
if (length(highlyCorDescr)!=0) {
  cat('\n', '2. Following Variables removed due to high correlation', '\n')
  cat(colnames(features)[highlyCorDescr])
  features <- features[,-highlyCorDescr]
} else cat('\n', "2. None of the variables removed due to high correlation", '\n')

#Remove Linear Dependencies in modeling data.
comboInfo <- findLinearCombos(features)
if (!is.null(comboInfo$remove)) {
  cat('\n', '3. Following Variables removed due to linear dependencies', '\n')
  cat(colnames(features)[comboInfo$remove])
  features <- features[, -comboInfo$remove]
} else cat('\n', "3. None of the variables removed due to linear dependencies", '\n')


#Split Modeling Data in Train and Test
set.seed(1000)
trainIndex <- createDataPartition(resp, p = split_ratio, list = FALSE, times = 1)
feat.Train <- features[ trainIndex,]
feat.Test  <- features[-trainIndex,]
resp.Train <- resp[trainIndex]
resp.Test <- resp[-trainIndex]
if(exists("my.weights")) use.weight <- my.weights[trainIndex]
  
#Pre-process Data to scale between 0 and 1
preProcValues <- preProcess(feat.Train, method = c("range"))
train.data <- predict(preProcValues, feat.Train)
test.data <- predict(preProcValues, feat.Test)

#Remaning data transformtions on scoring data
if (opt_score == "Y") {
  sdata <- sdata[ ,-nzv]
  sdata <- sdata[,-highlyCorDescr]
  sdata <- sdata[,-comboInfo$remove]
  sdata <- predict(preProcValues, sdata)
}







