##############################################################################
# Module: Response Propensity Model                                          #
# Code: Model                                                                #
# Objective: Run classification model, score data (if avail)                 #
# Author: Ashish Jha                                                         #
# Last Updated: 5/4/2017                                                     #
##############################################################################


set.seed(1000)
seeds <- vector(mode = "list", length = 26)
for(i in 1:25) seeds[[i]]<- sample.int(n=1000, 10)
seeds[[26]]<-sample.int(1000, 1)


sub_sampling <-  if (sub_sample %in% c("smote","up","down","rose")) sub_sample else NULL
numb <- if (val_method == "cv") 10 else 5
nrepeat <- if (val_method == "repeatedcv") 5 else 1

use.weight <-  if(exists("my.weights")) use.weight else NULL

result.summary <- ifelse(metric!="ROC",defaultSummary,twoClassSummary)

fitControl <- trainControl(method = val_method,
                           number = numb,
                           repeats = nrepeat,
                           seeds = seeds,
                           classProbs = TRUE,
                           summaryFunction = result.summary,
                           sampling = sub_sampling,
                           savePredictions = TRUE,
                           search = "random",allowParallel = FALSE)




cl <- makeCluster(detectCores())
registerDoParallel(cl)


thresholds <- seq(0.01,0.99,0.025)

stime = as.POSIXct(Sys.time())

#Run Models
  models <- list()
  algorithmList <- unlist(strsplit(lst_models,split=','))
  if(!exists("results.train")) results.train <- list()
  if(!exists("results.test")) results.test <- list()
  if(!exists("Roc.coords.train")) Roc.coords.train <- list()
  if(!exists("Roc.coords.test")) Roc.coords.test <- list()
  if(!exists("variable.imp")) variable.imp <- list()
  
  truth.train <- factor(resp.Train,levels=c(Event,nonEvent))
  truth.test <- factor(resp.Test,levels=c(Event,nonEvent))
  
  for (m in algorithmList){
    set.seed(5627)
    models[[m]] <- train(x=train.data, y=resp.Train, 
                                    method = m,
                                    weights = use.weight,
                                    metric = metric,
                                    tuneLength = 10,
                                    trControl = fitControl)
    
    results.train[[m]] <- createThresholdTable(models[[m]],train.data,truth.train,thresholds)
    results.test[[m]] <- createThresholdTable(models[[m]],test.data,truth.test,thresholds)
    Roc.coords.train[[m]] <- coords(results.train[[m]]$roc,"best",best.method="closest.topleft",ret=c("threshold","sensitivity","specificity","accuracy"))
    Roc.coords.test[[m]] <- coords(results.test[[m]]$roc,"best",best.method="closest.topleft",ret=c("threshold","sensitivity","specificity","accuracy"))
    variable.imp[[m]] <- calculateVarImp(models[[m]],m)
    
    plot(results.test[[m]]$roc,print.thres="best",print.thres.best.method="closest.topleft",
         col="green",identity.col="red",identity.lwd=2,print.auc=TRUE,print.auc.col="black",
         auc.polygon=TRUE,auc.polygon.col="lightyellow",mar=c(5,5,4,2)+0.1,mgp=c(3,1,0))
    title(main=list(paste0("ROC Curve:",m),cex=1.0,col="darkblue"),line=1)
    mtext("Test Data",cex=0.75,col="darkblue",line=0)
    
    dotchart(x=rev(variable.imp[[m]][1:20,2]),labels=rev(variable.imp[[m]][1:20,1]),
             xlim=c(0,100),bg="dark green",main=paste0("Variable Importance: ",m))
    
    write.csv(results.test[[m]]$cutOffTable,file=paste0(path1,"CutOffTable_",m,".csv"))
    write.csv(variable.imp[[m]],file=paste0(path1,"VariableImportance_",m,".csv"))
  }
  
  
stopCluster(cl)

performanceOnTrain <- modelPerformance(results.train,"Train")
performanceOnTest <- modelPerformance(results.test,"Test")
performanceByModel <- rbind(performanceOnTest,performanceOnTrain)
performanceByModel <- performanceByModel[order(performanceByModel$Model),]

write.csv(performanceByModel,file=paste0(path1,"PerformanceByModel.csv"),row.names = FALSE)

etime = as.POSIXct(Sys.time())
cat( '\n', paste( 'Model took', round( difftime( etime, stime, units = "mins"), 2), 'minutes' ), '\n' )


#Coorelations amongst model predictions to identify models to be ensembled

if(!exists("all.tlist")) all.tlist <- list()

for  (i in algorithmList) {
  all.tlist[[i]] <- caretModelSpec(method=models[[i]]$method, tuneGrid=models[[i]]$bestTune)
}

all.fitControl <- trainControl(method = val_method,
                              number = numb,
                              repeats = nrepeat,
                              index = models[[1]]$control$index,
                              classProbs = TRUE,
                              summaryFunction = result.summary,
                              sampling = sub_sampling,
                              savePredictions = "final",
                              search = "grid",allowParallel = FALSE)


cl <- makeCluster(detectCores())
registerDoParallel(cl)

set.seed(5627)

all.models <- caretList(x=train.data, y=resp.Train, trControl=all.fitControl,weights = use.weight,metric=metric, tuneList= all.tlist)

stopCluster(cl)

model.corrleations <- modelCor(resamples(all.models))

write.csv(model.corrleations,file=paste0(path1,"ModelCorrleations.csv"),row.names = FALSE)

 









