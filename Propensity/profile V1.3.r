#Profile test data


choose.algorithm <- profile_algo
cutOff <- profile_cutoff
choose.data <- profile_data
  
assign("mydata",get(choose.data))

if (c("score") %in% names(mydata)) mydata$score <- NULL
if (c("Predicted_Response") %in% names(mydata)) mydata$Predicted_Response <- NULL

mydata$score <- scoreData(choose.algorithm,mydata)

mydata$Predicted_Response <- 0
check <- mydata$score>=cutOff
if (sum(check)>0) {
  mydata[which(check),]$Predicted_Response <- event
  
  #Now Profile
  if (class(models[[choose.algorithm]])=="caretStack") {
    varsToProfile <- variable.imp[names(variable.imp) %in% mods]
    varsToProfile <- lapply(varsToProfile,renameVars,c("Variable","Importance"))
    varsToProfile <- do.call(rbind,varsToProfile)
    
  } else {
    varsToProfile <- variable.imp[names(variable.imp) %in% choose.algorithm]
    #names(varsToProfile) <- c("Variable","Importance")
    varsToProfile <- lapply(varsToProfile,renameVars,c("Variable","Importance"))
    varsToProfile <- do.call(rbind,varsToProfile)
  }
  
  varsToProfile <- varsToProfile[varsToProfile$Importance >= 5,]
  varsToProfile <- as.character(unique(varsToProfile$Variable))
  
  
  allCols <- getColsToProfile(varsToProfile)
  keepcols <- allCols[["keepcols"]]
  
  if (choose.data=="test.data")  {
    profile.data <- mdata[-trainIndex,keepcols,with=FALSE]
  } else {
    profile.data <- mdata[trainIndex,keepcols,with=FALSE]
  }
  
  
  for (k in allCols[["bin.cols"]]) {
    if(length(bins[[k]]) != 1) {
      profile.data <- profile.apply.bins.only(x=k,bin=bins[[k]][["ivtable"]],mydata=profile.data)
    }
  }
  
  binned.variables <- sapply(names(profile.data), function(x) {grepl("binned",x)})
  binned.variables <- names(profile.data)[binned.variables]
  
  allProfiles <- getProfile(fac.variables=allCols[["fac.cols"]],binned.variables=binned.variables,
                            num.variables=allCols[["num.cols"]],bin.variables=allCols[["bin.cols"]],
                            profile.data,depvar)
  
  #Now Plot on categorical variables
  plotData <- allProfiles[["catProfiles"]]
  varsToPlot <- unique(plotData[["Variables"]])
  catPlot <- list()
  
  for (i in varsToPlot){
    catPlot[[i]] <- ggplot(data=plotData[plotData$Variables==i,],aes(x=Values, y=Percent.Event)) + geom_bar(stat = "identity")
    catPlot[[i]] <- catPlot[[i]] + xlab(i) + ylab("Proportion of Event")
    print(catPlot[[i]])
  }

  #Now PLot on Continuous Variables
  plotData <- allProfiles[["conProfile"]]
  plotData <- melt(plotData, id=c("Variables")) 
  varsToPlot <- unique(plotData[["Variables"]])
  conPlot <- list()
  
  for (i in varsToPlot){
    conPlot[[i]] <- ggplot(data=plotData[plotData$Variables==i,],aes(x=variable, y=value)) + geom_bar(stat = "identity")
    conPlot[[i]] <- conPlot[[i]] + xlab(i) + ylab("Average Value")
    print(conPlot[[i]])
  }
  
} else {
  cat("All rows scored as non-Event. No profiling is possible.")
}












