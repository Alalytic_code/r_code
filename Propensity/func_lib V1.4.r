##############################################################################
# Objective: Define generic functions usable across modules                  #
##############################################################################

#function to extract list from string
str_to_lst = function(str) {
  return( unlist( as.list( strsplit( gsub( ' ', '', str), ',') ) ) )
}

#function to *append* an object to an R data file
append = function(x, file) {
  old.objects = load(file, new.env())
  save(list = c(old.objects, deparse(substitute(x))), file = file)
}

#function to apply log transformation
logtr = function(x) {
  return(log(x + 1))
}

#Function to calculate distance from Sensitivity = 1 and Specificity = 1
dis <- function(x) { res <- confusionMatrix(x,positive=Event); return((res$byClass[1]-1)^2+(res$byClass[2]-1)^2)}

#Function to calculate Sensitivity
return.sens <- function(x) { res <- confusionMatrix(x,positive=Event); return(res$byClass[1])}

#Function to calculate Specificity
return.spec <- function(x) { res <- confusionMatrix(x,positive=Event); return(res$byClass[2])}

#Function to calculate Accuracy
return.accu <- function(x) { res <- confusionMatrix(x,positive=Event); return(res$overall[1])}

#Function to subset models$pred with best tune
sub.dataset <- function(x,y) {
  x.names <- names(x)
  x.ncols <- ncol(x)
  
  for (i in 1:length(y)){
    temp.name <- x.names[x.ncols-i]
    x <- x[eval(as.name(temp.name))==y[temp.name][1,1],]
    print(i)
  }
  return(x)
}

#Creates bins and recodes continuous variables
create.bins <- function(x,y=depvar,mydata) {
  this.bin <- smbinning(df=mydata,y=y,x=x,p=0.05)
  print(this.bin)
  if (length(this.bin)==1) {
    return(mydata)
  } else {
    this.bin <- this.bin[["ivtable"]]
    newvar <- paste0(x,"binned")
    mydata[ ,c(newvar):="Missing"]
    l.length <- if (this.bin[nrow(this.bin)-1,1]=="Missing") nrow(this.bin)-2 else nrow(this.bin)-1
    for (i in 1:l.length){
      mydata[eval(parse(text=paste0(x,this.bin[i,1]))) & get(newvar) == "Missing",c(newvar):=as.factor(i)]
    }
    return(mydata)
  } 
  
}


#Only creates bins 
create.bins.only <- function(x,y=depvar,mydata) {
  this.bin <- smbinning(df=mydata,y=y,x=x,p=0.05)
  return(this.bin)
}


#Only apply bins 
apply.bins.only <- function(x,bin,mydata) {
    newvar <- paste0(x,"binned")
    mydata[ ,c(newvar):="Missing"]
    l.length <- if(bin[nrow(bin)-1,1]=="Missing") nrow(bin)-2 else nrow(bin)-1
    for (i in 1:l.length) {
      mydata[eval(parse(text = paste0(x,bin[i,1]))) & get(newvar) == "Missing", c(newvar):= as.factor(i)]
    }
    return(mydata)
}

#Only apply bins 
profile.apply.bins.only <- function(x,bin,mydata) {
  newvar <- paste0(x,"binned")
  mydata[ ,c(newvar):="Missing"]
  l.length <- if(bin[nrow(bin)-1,1]=="Missing") nrow(bin)-2 else nrow(bin)-1
  for (i in 1:l.length) {
    mydata[eval(parse(text = paste0(x,bin[i,1]))) & get(newvar) == "Missing", c(newvar):= as.factor(bin[i,1])]
  }
  return(mydata)
}

#Create table for selecting threshold and performance report of model
createThresholdTable <- function(x,y,z,w) {
  
  #x model y data z truth w thresholds
  preds <- list()
  xts <- list()
  disFromOneOne <- numeric()
  sens <- numeric()
  spec <- numeric()
  accu <- numeric()
  opt.threshold <- numeric()
  chooseCutOff <- list()
  
  if (!(c(0.5) %in% w)) { w <- c(w[which(w < 0.5)],0.5,w[which(w > 0.5)])}
  
  truth <- factor(z,levels=c(Event,nonEvent))
  if (class(x)=="train") { 
    probsEvent <- predict(x,y,type="prob")[,Event]
  } else {
    probsEvent <- predict(x,y,type="prob")
    }
  
  ROC <- roc(truth,probsEvent,levels = c(Event,nonEvent),direction = ">")
  
  preds <- lapply(w, function(x) {factor(ifelse(probsEvent > x,Event,nonEvent),levels=c(Event,nonEvent))})
  xts <- lapply(preds, function(x) {table(x,truth)})
  disFromOneOne <- sapply(xts,dis)
  sens <- sapply(xts,return.sens)
  spec <- sapply(xts,return.spec)
  accu <- sapply(xts,return.accu)
  opt.threshold <- w[which.min(disFromOneOne)]
  
  chooseCutOff <- data.frame(Thresholds = w,Sensitivity = sens, Specificity = spec, Distance = disFromOneOne, Accuracy = accu)
  
  sens <- return.sens(xts[[which(w==0.5)]])
  spec <- return.spec(xts[[which(w==0.5)]])
  accu <- return.accu(xts[[which(w==0.5)]])
  
  mylist <- list(sens=sens,spec=spec,roc=ROC,accu=accu,cutOffTable=chooseCutOff,optimumThreshold=opt.threshold)
  
  return(mylist)
}

#Calculate variable importance
calculateVarImp <- function(x,m) {
  variable.importance <- varImp(x)$importance
  variable.importance$Var.Name <- row.names(variable.importance)
  variable.importance <- variable.importance[order(-variable.importance[,1]),]
  variable.importance <- data.frame(variable.importance$Var.Name,variable.importance[[1]])
  colnames(variable.importance) <- c(paste0("Var","_",m),paste0("Imp","_",m))
  
  return(variable.importance)
}


#Summarize model performace
modelPerformance <- function(x,datasetName) {
  Model <- names(x)
  Sensitivity <- unname(sapply(x, function(y) { as.numeric(y$sens)}))
  Specificity <- unname(sapply(x,function(y) { as.numeric(y$spec)}))
  Accuracy <- unname(sapply(x,function(y) { as.numeric(y$accu)}))
  ROC <- unname(sapply(x,function(y) { as.numeric(y$roc$auc)}))
  ModelsEnsembled <- ifelse(grepl("\\+",Model,perl = TRUE),"Yes","No")
  perf <- data.frame(Model=Model,Dataset=datasetName,ROC=ROC,Sensitivity=Sensitivity,Specificity=Specificity,Accuracy=Accuracy,ModelsEnsembled=ModelsEnsembled)
  return(perf)
}

#Score Dataset
scoreData <- function(choose.algorithm,scoringData) {
  if (class(models[[choose.algorithm]])=="caretStack") { 
    return(predict(models[[choose.algorithm]],scoringData,type="prob"))
  } else {
    return(predict(models[[choose.algorithm]],scoringData,type="prob")[,Event])
  }
}

#Get Variables to Profile dataset. y varsToProfile
getColsToProfile <- function(y) {
  
  fac.pos <- sapply(y, function(x) {grepl("\\.",x)})
  if(length(fac.pos[fac.pos==TRUE])>0) {
    fac.variables <- y[fac.pos]
    fac.variables <- unique(sapply(fac.variables,function(x) unlist(strsplit(as.character(x),split = "\\.")))[1,])
  } else {
    fac.variables <- character()
  }

  y <- y[!fac.pos]
  
  bin.pos <- sapply(y, function(x) {grepl("binned",x)})
  if(length(bin.pos[bin.pos==TRUE])>0) {
    bin.variables <- y[bin.pos]
    bin.variables <- unique(sapply(bin.variables,function(x) unlist(strsplit(as.character(x),split = "binned")))[1,])
  } else {
    bin.variables <- character()
  }

  num.variables <- y[!bin.pos]
  keepcols <- c(depvar,fac.variables,bin.variables,num.variables)
  allCols <- list(keepcols=keepcols,fac.cols=fac.variables,bin.cols=bin.variables,num.cols=num.variables)
  
  return(allCols)
}

#Get Profiles on categorical and continuous variables
getProfile <- function(fac.variables,binned.variables,num.variables,bin.variables,profile.data,depvar) {
  #Profile by categorical variables
  propEvent <- list()
  propEvent <- lapply(c(fac.variables,binned.variables),function(x) {data.frame(prop.table(table(profile.data[[x]],profile.data[[depvar]]),1))})
  names(propEvent) <- c(fac.variables,binned.variables)
  propEvent <- do.call(rbind,propEvent)
  propEvent <- propEvent[propEvent$Var2==1,]
  propEvent$Var2 <- NULL 
  names(propEvent) <- c("Values","Percent.Event")
  myVars <- sapply(row.names(propEvent),function(x) unlist(strsplit(as.character(x),split = "binned")))
  myVars <- unname(sapply(myVars,`[[`,1))
  myVars <- lapply(myVars,function(x) unlist(strsplit(as.character(x),split = "\\.")))
  myVars <- unname(sapply(myVars,`[[`,1))
  propEvent$Variables <- myVars
  
  #Profile by continuous variables
  meanEvent <- sapply(unique(c(num.variables,bin.variables)),function(x) tapply(profile.data[[x]],profile.data[[depvar]],mean,na.rm=T))
  meanEvent <- data.frame(t(meanEvent))
  if (ncol(meanEvent)==2) {
    names(meanEvent) <- c("NonEvent","Event")
    meanEvent$Variables <- row.names(meanEvent)
  } 

  allProfiles <- list(catProfiles=propEvent,conProfile=meanEvent)
  return(allProfiles)
}


#Renames columns of a data frame
renameVars <- function(x,y) { names(x) <- y; return(x)} # x dataframe y vector


