
mods <- unlist(strsplit(en_mods,split=','))  
  
en.models <- all.models[names(all.models) %in% mods]
class(en.models) <- "caretList"

en.fitControl <- trainControl(
  number = numb,
  classProbs = TRUE,
  summaryFunction = result.summary,
  savePredictions = "final"
)

#en_type <- c("Ensemble_glm","Ensemble_gbm","Ensemble_rf")
en_type <- paste0(paste0(mods,collapse = "+"),c("_en_glm","_en_gbm","_en_rf"))
en_models <- c("glm","gbm","rf")  


cl <- makeCluster(detectCores())
registerDoParallel(cl)

for (i in en_type) {
  set.seed(5627)
  models[[i]] <- caretStack(en.models, method=en_models[which(en_type==i)], metric=metric, trControl=en.fitControl)
  results.train[[i]] <- createThresholdTable(models[[i]],train.data,truth.train,thresholds)
  results.test[[i]] <- createThresholdTable(models[[i]],test.data,truth.test,thresholds)
  Roc.coords.train[[i]] <- coords(results.train[[i]]$roc,"best",best.method="closest.topleft",ret=c("threshold","sensitivity","specificity","accuracy"))
  Roc.coords.test[[i]] <- coords(results.test[[i]]$roc,"best",best.method="closest.topleft",ret=c("threshold","sensitivity","specificity","accuracy"))
  
  plot(results.test[[i]]$roc,print.thres="best",print.thres.best.method="closest.topleft",
       col="green",identity.col="red",identity.lwd=2,print.auc=TRUE,print.auc.col="black",
       auc.polygon=TRUE,auc.polygon.col="lightyellow",mar=c(5,5,4,2)+0.1,mgp=c(3,1,0))
  title(main=list(paste0("ROC Curve:",i),cex=1.0,col="darkblue"),line=1)
  mtext("Test Data",cex=0.75,col="darkblue",line=0)
  
  
  write.csv(results.test[[i]]$cutOffTable,file=paste0(path1,"CutOffTable_",i,".csv"))
  
}

stopCluster(cl)

en.performanceOnTrain <- modelPerformance(results.train,"Train")
en.performanceOnTest <- modelPerformance(results.test,"Test")
en.performanceByModel <- rbind(en.performanceOnTest,en.performanceOnTrain)
en.performanceByModel <- en.performanceByModel[order(en.performanceByModel$Model),]
#en.performanceByModel$ModelsEnsembled <- paste0(mods,collapse = "+")
#en.performanceByModel$ModelsEnsembled <- ifelse(grepl("\\+",en.performanceByModel$Model,perl = TRUE),paste0(mods,collapse = "+"),"Not Applicable")

write.csv(en.performanceByModel,file=paste0(path1,"EnsemblePerformanceByModel.csv"),row.names = FALSE)



   

