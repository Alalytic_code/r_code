##############################################################################
# Module: Propensity Model                                          #
# Code: Source                                                               #
# Objective: Capture user inputs, prepare env, run module scripts            #
# Author: Ashish Jha                                                         #
# Last Updated: 5/4/2017                                                     #
##############################################################################
#Try Adaptive resampling. Before that complete ensemble model.
#Change 0-1 scaling to normalization and range scaling minus mean and division by range
#Add PCA. Normalization and scaling should be done before PCA
#Add feature transformation later: At the end. Maybe feature transformation should always be followed by PCA.
options(scipen=99)
options(digits=4)
options(max.print=1000)
rm(list=ls())

#install required packages where not installed - uncomment for production deployment
for (package in c('data.table', 'plyr', 'dplyr', 'car', 'ROCR', 'randomForest', 'xgboost', 'caTools','caret','caretEnsemble','doParallel','pROC')) 
{
  if (!require(package, character.only=T, quietly=T)) 
  {
    install.packages(package)
  }
}

suppressMessages(library(data.table))     #fread
suppressMessages(library(plyr))
suppressMessages(library(dplyr))
suppressMessages(library(car))            #VIF
suppressMessages(library(ROCR))           #ROCR
suppressMessages(library(randomForest))
suppressMessages(library(xgboost))
suppressMessages(library(caTools))        #sample.split

suppressMessages(library(doParallel))
suppressMessages(library(pROC))
suppressMessages(library(lattice))
suppressMessages(library(randomGLM))
suppressMessages(library(fastAdaboost))
suppressMessages(library(C50))
suppressMessages(library(RRF))
suppressMessages(library(smbinning))
suppressMessages(library(tcltk))
suppressMessages(library(gridExtra))
suppressMessages(library(kernlab))
suppressMessages(library(caret)) 
suppressMessages(library(caretEnsemble))

args = commandArgs (trailingOnly = TRUE)

# #L1 (most generic) inputs
# run         = args[1]        #100 = run only read, 110 = run read & prep, 111 = run read, prep & model etc...
# path1       = args[2]        #input & output path
# file1       = args[3]        #input data file
# #L2 inputs
# opt_score   = args[4]        #score a dataset? (Y/N)
# file2       = args[5]        #scoring data file
# depvar      = args[6]        #name of the target variable
# lst_dim     = args[7]        #list of descriptor columns
# #L3 (most specific) inputs
# prob_true   = args[8]        #predicted probability greater than this will be considered '1'
# opt_split   = args[9]        #create out-sample validation data? (Y/N)
# split_ratio = args[10]       #percent of rows to include in out-sample
# rf_ntree    = args[11]       #random forest 'ntree' parameter (impacts computation time)
# rf_mtry     = args[12]       #random forest 'mtry' parameter
# gb_nrounds  = args[13]       #gradient boost 'nrounds' parameter (impacts computation time)

######################## test values - uncomment for testing ##############################
run         = '000'
path1       = 'D:/Products/Response/'
file1       = 'data_input.csv'
opt_score   = 'Y'
file2       = 'data_score.csv'
depvar      = 'Response'
event       = '1'
case.weight = ''
lst_dim     = 'Zip_Code'
make_factor = 'Specialty,In_Call_Plan,Prior_Response'
split_ratio = 0.1
sub_sample  = "smote"
lst_models  = 'xgbLinear,svmRadial'
en_mods     = 'xgbLinear,svmRadial'
Binning     = 'Y'
val_method  = 'repeatedcv'
metric      = 'ROC' #Choose between Accuracy, Kappa and ROC
score_algo  = 'svmRadial'
score_cutoff= 0.453
profile_algo= 'svmRadial'
profile_cutoff = 0.453
profile_data= 'test.data'
#Does not support randomGLM, deepboost#
#Supports xgbLinear xgbTree rf glmnet svmRadial adaboost LogitBoost RRFglobal#
###########################################################################################

#set input and output paths
setwd (path1)
dir.create('data/out', showWarnings = FALSE)


#load generic functions into env
source('code/V1.3/func_lib V1.4.R', local = TRUE, verbose = FALSE, print.eval = TRUE, echo = FALSE)

#run Read
if (substr(run,1,1) == '1') {  
  source('code/V1.3/new read code V1.3.R', local = TRUE, verbose = FALSE, print.eval = TRUE, echo = FALSE)
}

#prep Data
if (substr(run,2,2) == '1') {  
  source('code//V1.3/new prep code V1.3.R', local = TRUE, verbose = FALSE, print.eval = TRUE, echo = FALSE)
}

#run Model
if (substr(run,3,3) == '1') {  
  source('code/V1.3/model_resp V1.3.R', local = TRUE, verbose = FALSE, print.eval = TRUE, echo = FALSE)
}

#run Ensemble
if (substr(run,4,4) == '1') {  
  source('code/V1.3/en_model_resp V1.3.R', local = TRUE, verbose = FALSE, print.eval = TRUE, echo = FALSE)
}

#run Score
if (substr(run,5,5) == '1') {  
  source('code/V1.3/score V1.3.R', local = TRUE, verbose = FALSE, print.eval = TRUE, echo = FALSE)
}

#run Profile
if (substr(run,6,6) == '1') {  
  source('code/V1.3/profile V1.3.R', local = TRUE, verbose = FALSE, print.eval = TRUE, echo = FALSE)
}