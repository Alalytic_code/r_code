##############################################################################
# Module: Response Propensity Model                                          #
# Code: Read                                                                 #
# Objective: Read input data, print basic diagnostics                        #
# Author: Ashish Jha                                                         #
# Last Updated: 5/4/2017                                                     #
##############################################################################

stime = as.POSIXct(Sys.time())
cat('\n', '***** CONSOLE OUTPUT: READ ***** ', '\n')

#read input file
mdata = fread( paste0('data/', file1))
cat( '\n', paste( 'input data file: ', path1, '/data/', file1, sep = ''), '\n')
cat( paste( nrow(mdata), 'records read from file'))

#mdata[is.na(mdata)] <- 0

if (case.weight %in% names(mdata)) {
  my.weights <- as.numeric(mdata[[case.weight]])
  mdata <- mdata[ ,eval(case.weight):=NULL]
}


lst_dim <- unlist(strsplit(lst_dim,split=','))
col.classes <- sapply(mdata,class)
col.classes <- col.classes[!(names(col.classes) %in% lst_dim)]

make_factor <- unlist(strsplit(make_factor,split=','))

lst_factor <- names(col.classes[col.classes=="character"])
lst_factor <- c(lst_factor,make_factor)

lst_number <- names(col.classes[col.classes=="integer" | col.classes=="numeric"])
lst_number <- lst_number[!(lst_number %in% make_factor)]

all.vars <- c(lst_factor,lst_number)

mdata <- mdata[ ,all.vars,with=FALSE]

mdata[ ,c(lst_number) := lapply(.SD,as.numeric),.SDcols=lst_number]
mdata[ ,c(lst_factor) := lapply(.SD,FUN= function(x) as.factor(x)),.SDcols=lst_factor]
mdata[ ,c(lst_factor) := lapply(.SD,FUN= function(x) addNA(x)),.SDcols=lst_factor]
mdata[ ,c(lst_factor) := lapply(.SD,FUN= function(x) as.factor(ifelse(is.na(as.character(x)),"Missing",as.character(x)))),.SDcols=lst_factor]


if (opt_score == "Y") {
#Read scoring data
sdata = fread( paste0('data/', file2))
cat( '\n', paste( 'scoring data file: ', path1, '/data/', file1, sep = ''), '\n')
cat( paste( nrow(sdata), 'records read from file'))

if(exists("my.weights")) sdata <- sdata[ ,eval(case.weight):=NULL]

#Keep relevant variables in scoring data
sdata[[depvar]] <- 0
sdata <- sdata[ ,all.vars,with=FALSE]

#Other data treatments now
sdata[ ,c(lst_number) := lapply(.SD,as.numeric),.SDcols=lst_number]
sdata[ ,c(lst_factor) := lapply(.SD,FUN= function(x) as.factor(x)),.SDcols=lst_factor]
sdata[ ,c(lst_factor) := lapply(.SD,FUN= function(x) addNA(x)),.SDcols=lst_factor]
sdata[ ,c(lst_factor) := lapply(.SD,FUN= function(x) as.factor(ifelse(is.na(as.character(x)),"Missing",as.character(x)))),.SDcols=lst_factor]

}
