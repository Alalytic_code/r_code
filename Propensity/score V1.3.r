#Apply model to scoring data
if (exists("sdata")) {
  if (c("score") %in% names(sdata)) sdata$score <- NULL
  if (c("Predicted_Response") %in% names(sdata)) sdata$Predicted_Response <- NULL

  choose.algorithm <- score_algo
  cutOff <- score_cutoff
    
  sdata$score <- scoreData(choose.algorithm,sdata)
  
  sdata$Predicted_Response <- 0
  check <- sdata$score>=cutOff
  if (sum(check)>0) sdata[which(check),]$Predicted_Response <- event
  write.csv(sdata,file=paste0(path1,"scoredData.csv"),row.names = FALSE)
  
  
} else {
  cat("Scoring dataset has not been read yet.")
}
